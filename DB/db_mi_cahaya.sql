-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06 Feb 2019 pada 17.48
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mi_cahaya`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(60) DEFAULT 'no_photo.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `judul`, `deskripsi`, `foto`) VALUES
(1, 'testing', 'ini adalah pengujian\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'IMG_file_1513828367.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto`
--

CREATE TABLE `foto` (
  `id_foto` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `alamat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `foto`
--

INSERT INTO `foto` (`id_foto`, `nama`, `jenis`, `alamat`) VALUES
(8, 'store_page.png', 'berita', 'C:/Ampps/www/MI/CI-Web/uploads/'),
(9, 'TugasKeDuaJava.PNG', 'berita', 'C:/Ampps/www/MI/CI-Web/uploads/TugasKeDuaJava.PNG'),
(10, 'TugasKeDuaXml.PNG', 'pengumuman', 'C:/Ampps/www/MI/CI-Web/uploads/TugasKeDuaXml.PNG'),
(11, 'stackoverflow.PNG', 'galeri', 'C:/Ampps/www/MI/CI-Web/uploads/stackoverflow.PNG'),
(13, 'stackoverflow1.PNG', 'berita', 'C:/Ampps/www/MI/CI-Web/uploads/stackoverflow1.PNG'),
(14, 'admin_page2.png', 'berita', 'C:/Ampps/www/MI/CI-Web/uploads/admin_page2.png'),
(15, 'ss_postman.PNG', 'pengumuman', 'C:/Ampps/www/MI/CI-Web/uploads/ss_postman.PNG'),
(16, 'MainTestSoal2.PNG', 'pengumuman', 'C:/Ampps/www/MI/CI-Web/uploads/MainTestSoal2.PNG'),
(17, 'dinar.jpg', 'pengumuman', 'C:/Ampps/www/MI/CI-Web/uploads/dinar.jpg'),
(18, 'dinar1.jpg', 'pengumuman', 'C:/Ampps/www/MI/CI-Web/uploads/dinar1.jpg'),
(19, 'dinar2.jpg', 'pengumuman', 'C:/Ampps/www/MI/CI-Web/uploads/dinar2.jpg'),
(20, 'dinar3.jpg', 'pengumuman', 'C:/Ampps/www/MI/CI-Web/uploads/dinar3.jpg'),
(21, 'dinar4.jpg', 'berita', 'C:/Ampps/www/MI/CI-Web/uploads/dinar4.jpg'),
(22, 'Kelas_TransaksiMatKul.PNG', 'berita', 'C:/Ampps/www/MI/CI-Web/uploads/Kelas_TransaksiMatKul.PNG'),
(23, 'IMG_file_1503854265.png', 'berita', 'C:/Ampps/www/MI/CI-Web/uploads/IMG_file_1503854265.png'),
(24, 'IMG_file_1504581197.jpg', 'berita', 'C:/xampp/htdocs/MI/CI-Web/uploads/IMG_file_1504581197.jpg'),
(26, 'manfaat_kulit_jeruk.jpg', 'pengumuman', 'C:/xampp/htdocs/MI/mi/uploads/manfaat_kulit_jeruk.jpg'),
(27, 'IMG_file_1510123546.jpg', 'berita', 'C:/xampp/htdocs/MI/mi/uploads/IMG_file_1510123546.jpg'),
(28, '1.jpg', 'pengumuman', 'C:/xampp/htdocs/MI/mi/uploads/1.jpg'),
(29, 'a.jpg', 'pengumuman', 'C:/xampp/htdocs/MI/mi/uploads/a.jpg'),
(30, 'IMG_file_1513828210.jpg', 'berita', 'C:/xampp/htdocs/MI/mi/uploads/IMG_file_1513828210.jpg'),
(31, 'IMG_file_1513828367.png', 'berita', 'C:/xampp/htdocs/MI/mi/uploads/IMG_file_1513828367.png'),
(32, '3.jpg', 'pengumuman', 'C:/xampp/htdocs/MI/mi/uploads/3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri`
--

CREATE TABLE `galeri` (
  `id_galeri` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `caption` text NOT NULL,
  `path` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `galeri`
--

INSERT INTO `galeri` (`id_galeri`, `nama`, `caption`, `path`) VALUES
(2, 'IMG_.jpg', 'alam adalah lingkungan lol', 'C:/xampp/htdocs/MI/mi/uploads/IMG_.jpg'),
(3, 'IMG_1.jpg', 'lion king lorem', 'C:/xampp/htdocs/MI/mi/uploads/IMG_1.jpg'),
(4, 'IMG_2.jpg', 'television', 'C:/xampp/htdocs/MI/mi/uploads/IMG_2.jpg'),
(5, 'IMG_3.jpg', 'lakshdlaksndlasm alc\r\n        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'C:/xampp/htdocs/MI/mi/uploads/IMG_3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kotak_masuk`
--

CREATE TABLE `kotak_masuk` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `username` varchar(50) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kotak_masuk`
--

INSERT INTO `kotak_masuk` (`id`, `nama`, `email`, `username`, `no_telp`, `pesan`) VALUES
(1, 'Lut Dinar Fadila', 'lutdinar@icloud.com', '', '085721089949', 'Penerimaan siswa baru'),
(2, 'Lut Dinar Fadila', 'lutdinar@icloud.com', 'lutdinar', '-', 'Lupa Password');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(60) DEFAULT 'no_photo.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `judul`, `deskripsi`, `foto`) VALUES
(1, 'Kegiatan Memperingati HUT Kemerdekaan RI ke 72', 'akan dilakukan upacara pada jam 7.00', '1.jpg'),
(2, 'Cahaya Qurban 1438 H', 'MI cahaya akan melakukan qurban', 'a.jpg'),
(3, 'testing', 'test', '3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_catatan`
--

CREATE TABLE `tb_catatan` (
  `id_catatan` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `catatan_ortu` varchar(255) NOT NULL,
  `catatan_guru` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_catatan`
--

INSERT INTO `tb_catatan` (`id_catatan`, `id_kegiatan`, `catatan_ortu`, `catatan_guru`) VALUES
(1, 401, 'tolong ajari cara baca al-quran', 'baik akan saya ajari'),
(2, 402, 'terima kasih sudah mau mengajari', 'sama sama'),
(3, 403, 'halo', 'halo'),
(4, 404, 'apa kabar', 'baik'),
(5, 405, 'hey', 'hey juga'),
(6, 406, 'test', 'testing'),
(8, 710, 'terima kasih', '2'),
(9, 711, 'terima kasih', '3'),
(10, 713, 'terima kasih', '4'),
(14, 717, 'Thanks', ''),
(20, 723, 'terima kasih banyak - nuhun 1', '99'),
(25, 728, ':(', ''),
(26, 729, 'Qwerty', ''),
(27, 730, '2', ''),
(28, 731, 'I', ''),
(29, 732, 'Yui', ''),
(30, 733, 'Test', ''),
(31, 734, 'Ty', ''),
(32, 735, 'Asd', ''),
(33, 736, '', ''),
(40, 743, 'terima kasih', ''),
(42, 745, '', ''),
(43, 746, '', ''),
(46, 749, 'terima kasih', ''),
(48, 751, 'terima kasih', ''),
(60, 763, '', ''),
(61, 764, '', ''),
(62, 765, '', ''),
(63, 766, '', ''),
(64, 767, '', ''),
(65, 768, '', 'Siap'),
(69, 772, '', ''),
(70, 773, 'baik2 saja', 'Ok'),
(72, 775, '', 'admin'),
(73, 776, '', ''),
(74, 777, 'Terima kasih', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_guru`
--

CREATE TABLE `tb_guru` (
  `id_guru` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `nuptk` varchar(20) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `ttl` varchar(60) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `agama` varchar(8) NOT NULL,
  `foto` varchar(60) DEFAULT 'no_photo.jpg',
  `id_kelas` int(2) DEFAULT NULL,
  `jabatan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_guru`
--

INSERT INTO `tb_guru` (`id_guru`, `id_user`, `nip`, `nuptk`, `nik`, `nama`, `ttl`, `jenis_kelamin`, `alamat`, `agama`, `foto`, `id_kelas`, `jabatan`) VALUES
(74, 256, '1234567891011', '123456789', '1231234564567', 'dinar', 'cibiru, 12 April 1994', 'L', 'cibiru no 44', 'islam', 'no_photo', 1, 'guru');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kegiatan`
--

CREATE TABLE `tb_kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kegiatan`
--

INSERT INTO `tb_kegiatan` (`id_kegiatan`, `id_siswa`, `tanggal`) VALUES
(401, 182, '2013-01-01'),
(402, 182, '2013-01-02'),
(403, 182, '2013-01-03'),
(404, 182, '2013-01-04'),
(405, 182, '2013-01-05'),
(406, 182, '2013-01-06'),
(407, 182, '2013-01-07'),
(408, 182, '2013-01-08'),
(409, 182, '2013-01-09'),
(410, 182, '2013-01-10'),
(411, 182, '2013-01-11'),
(412, 182, '2013-01-12'),
(413, 182, '2013-01-13'),
(414, 182, '2013-01-14'),
(415, 182, '2013-01-15'),
(416, 182, '2013-01-16'),
(417, 182, '2013-01-17'),
(418, 182, '2013-01-18'),
(419, 182, '2013-01-19'),
(420, 182, '2013-01-20'),
(421, 182, '2013-01-21'),
(422, 182, '2013-01-22'),
(423, 182, '2013-01-23'),
(424, 182, '2013-01-24'),
(425, 182, '2013-01-25'),
(426, 182, '2013-01-26'),
(427, 182, '2013-01-27'),
(428, 182, '2013-01-28'),
(429, 182, '2013-01-29'),
(430, 182, '2013-01-30'),
(431, 182, '2013-01-31'),
(432, 182, '2018-12-01'),
(433, 182, '2018-12-01'),
(434, 182, '2018-12-01'),
(435, 182, '2018-12-02'),
(436, 182, '2018-12-02'),
(437, 182, '2018-12-02'),
(438, 182, '2018-12-02'),
(439, 182, '2018-12-02'),
(440, 182, '2018-12-02'),
(441, 182, '2018-12-02'),
(442, 182, '2018-12-02'),
(443, 182, '2018-12-02'),
(444, 182, '2018-12-02'),
(445, 182, '2018-12-02'),
(446, 182, '2018-12-02'),
(448, 182, '2018-12-02'),
(449, 182, '2018-12-02'),
(450, 182, '2018-12-02'),
(451, 182, '2018-12-02'),
(452, 182, '2018-12-02'),
(453, 182, '2018-12-02'),
(454, 182, '2018-12-02'),
(455, 182, '2018-12-02'),
(456, 182, '2018-12-02'),
(457, 182, '2018-12-02'),
(458, 182, '2018-12-02'),
(459, 182, '2018-12-02'),
(460, 182, '2018-12-02'),
(461, 182, '2018-12-02'),
(462, 182, '2018-11-24'),
(463, 182, '2018-11-24'),
(464, 182, '2018-11-24'),
(465, 182, '2018-11-30'),
(466, 182, '2018-11-30'),
(467, 182, '2018-11-30'),
(468, 182, '2018-11-24'),
(469, 182, '2018-11-24'),
(470, 182, '2018-11-24'),
(471, 182, '2018-11-24'),
(472, 182, '2018-11-24'),
(473, 182, '2018-11-24'),
(474, 182, '2018-11-25'),
(475, 182, '2018-11-25'),
(476, 182, '2018-11-25'),
(477, 182, '2018-11-25'),
(478, 182, '2018-11-30'),
(479, 182, '2018-12-02'),
(480, 182, '2018-12-02'),
(481, 182, '2018-11-25'),
(482, 182, '2018-11-25'),
(483, 182, '2018-11-30'),
(484, 182, '2018-11-25'),
(485, 182, '2018-11-25'),
(486, 182, '2018-11-25'),
(487, 182, '2018-11-26'),
(488, 183, '2013-01-01'),
(489, 183, '2013-01-02'),
(490, 183, '2013-01-03'),
(491, 183, '2013-01-04'),
(492, 183, '2013-01-05'),
(493, 183, '2013-01-06'),
(494, 183, '2013-01-07'),
(495, 183, '2013-01-08'),
(496, 183, '2013-01-09'),
(497, 183, '2013-01-10'),
(498, 183, '2013-01-11'),
(499, 183, '2013-01-12'),
(500, 183, '2013-01-13'),
(501, 183, '2013-01-14'),
(502, 183, '2013-01-15'),
(503, 183, '2013-01-16'),
(504, 183, '2013-01-17'),
(505, 183, '2013-01-18'),
(506, 183, '2013-01-19'),
(507, 183, '2013-01-20'),
(508, 183, '2013-01-21'),
(509, 183, '2013-01-22'),
(510, 183, '2013-01-23'),
(511, 183, '2013-01-24'),
(512, 183, '2013-01-25'),
(513, 183, '2013-01-26'),
(514, 183, '2013-01-27'),
(515, 183, '2013-01-28'),
(516, 183, '2013-01-29'),
(517, 183, '2013-01-30'),
(518, 183, '2013-01-31'),
(519, 184, '2013-01-01'),
(520, 184, '2013-01-02'),
(521, 184, '2013-01-03'),
(522, 184, '2013-01-04'),
(523, 184, '2013-01-05'),
(524, 184, '2013-01-06'),
(525, 184, '2013-01-07'),
(526, 184, '2013-01-08'),
(527, 184, '2013-01-09'),
(528, 184, '2013-01-10'),
(529, 184, '2013-01-11'),
(530, 184, '2013-01-12'),
(531, 184, '2013-01-13'),
(532, 184, '2013-01-14'),
(533, 184, '2013-01-15'),
(534, 184, '2013-01-16'),
(535, 184, '2013-01-17'),
(536, 184, '2013-01-18'),
(537, 184, '2013-01-19'),
(538, 184, '2013-01-20'),
(539, 184, '2013-01-21'),
(540, 184, '2013-01-22'),
(541, 184, '2013-01-23'),
(542, 184, '2013-01-24'),
(543, 184, '2013-01-25'),
(544, 184, '2013-01-26'),
(545, 184, '2013-01-27'),
(546, 184, '2013-01-28'),
(547, 184, '2013-01-29'),
(548, 184, '2013-01-30'),
(549, 184, '2013-01-31'),
(550, 182, '2018-11-28'),
(551, 182, '2018-11-30'),
(552, 182, '2018-11-30'),
(553, 183, '2013-11-01'),
(554, 183, '2013-11-02'),
(555, 183, '2013-11-03'),
(556, 183, '2013-11-04'),
(557, 183, '2013-11-05'),
(558, 183, '2013-11-06'),
(559, 183, '2013-11-07'),
(560, 183, '2013-11-08'),
(561, 183, '2013-11-09'),
(562, 183, '2013-11-10'),
(563, 183, '2013-11-11'),
(564, 183, '2013-11-12'),
(565, 183, '2013-11-13'),
(566, 183, '2013-11-14'),
(567, 183, '2013-11-15'),
(568, 183, '2013-11-16'),
(569, 183, '2013-11-17'),
(570, 183, '2013-11-18'),
(571, 183, '2013-11-19'),
(572, 183, '2013-11-20'),
(573, 183, '2013-11-21'),
(574, 183, '2013-11-22'),
(575, 183, '2013-11-23'),
(576, 183, '2013-11-24'),
(577, 183, '2013-11-25'),
(578, 183, '2013-11-26'),
(579, 183, '2013-11-27'),
(580, 183, '2013-11-28'),
(581, 183, '2013-11-29'),
(582, 183, '2013-11-30'),
(583, 184, '2013-11-01'),
(584, 184, '2013-11-02'),
(585, 184, '2013-11-03'),
(586, 184, '2013-11-04'),
(587, 184, '2013-11-05'),
(588, 184, '2013-11-06'),
(589, 184, '2013-11-07'),
(590, 184, '2013-11-08'),
(591, 184, '2013-11-09'),
(592, 184, '2013-11-10'),
(593, 184, '2013-11-11'),
(594, 184, '2013-11-12'),
(595, 184, '2013-11-13'),
(596, 184, '2013-11-14'),
(597, 184, '2013-11-15'),
(598, 184, '2013-11-16'),
(599, 184, '2013-11-17'),
(600, 184, '2013-11-18'),
(601, 184, '2013-11-19'),
(602, 184, '2013-11-20'),
(603, 184, '2013-11-21'),
(604, 184, '2013-11-22'),
(605, 184, '2013-11-23'),
(606, 184, '2013-11-24'),
(607, 184, '2013-11-25'),
(608, 184, '2013-11-26'),
(609, 184, '2013-11-27'),
(610, 184, '2013-11-28'),
(611, 184, '2013-11-29'),
(612, 184, '2013-11-30'),
(613, 182, '2013-11-01'),
(614, 182, '2013-11-02'),
(615, 182, '2013-11-03'),
(616, 182, '2013-11-04'),
(617, 182, '2013-11-05'),
(618, 182, '2013-11-06'),
(619, 182, '2013-11-07'),
(620, 182, '2013-11-08'),
(621, 182, '2013-11-09'),
(622, 182, '2013-11-10'),
(623, 182, '2013-11-11'),
(624, 182, '2013-11-12'),
(625, 182, '2013-11-13'),
(626, 182, '2013-11-14'),
(627, 182, '2013-11-15'),
(628, 182, '2013-11-16'),
(629, 182, '2013-11-17'),
(630, 182, '2013-11-18'),
(631, 182, '2013-11-19'),
(632, 182, '2013-11-20'),
(633, 182, '2013-11-21'),
(634, 182, '2013-11-22'),
(635, 182, '2013-11-23'),
(636, 182, '2013-11-24'),
(637, 182, '2013-11-25'),
(638, 182, '2013-11-26'),
(639, 182, '2013-11-27'),
(640, 182, '2013-11-28'),
(641, 182, '2013-11-29'),
(642, 182, '2013-11-30'),
(643, 183, '2018-12-01'),
(644, 183, '2018-12-02'),
(645, 183, '2018-12-03'),
(646, 183, '2018-12-04'),
(647, 183, '2018-12-05'),
(648, 183, '2018-12-06'),
(649, 183, '2018-12-07'),
(650, 183, '2018-12-08'),
(651, 183, '2018-12-09'),
(652, 183, '2018-12-10'),
(653, 183, '2018-12-11'),
(654, 183, '2018-12-12'),
(655, 183, '2018-12-13'),
(656, 183, '2018-12-14'),
(657, 183, '2018-12-15'),
(658, 183, '2018-12-16'),
(659, 183, '2018-12-17'),
(660, 183, '2018-12-18'),
(661, 183, '2018-12-19'),
(662, 183, '2018-12-20'),
(663, 183, '2018-12-21'),
(664, 183, '2018-12-22'),
(665, 183, '2018-12-23'),
(666, 183, '2018-12-24'),
(667, 183, '2018-12-25'),
(668, 183, '2018-12-26'),
(669, 183, '2018-12-27'),
(670, 183, '2018-12-28'),
(671, 183, '2018-12-29'),
(672, 183, '2018-12-30'),
(673, 183, '2018-12-31'),
(674, 183, '2018-02-06'),
(676, 183, '2018-02-03'),
(677, 182, '2013-04-01'),
(678, 182, '2013-04-02'),
(679, 182, '2013-04-03'),
(680, 182, '2013-04-04'),
(681, 182, '2013-04-05'),
(682, 182, '2013-04-06'),
(683, 182, '2013-04-07'),
(684, 182, '2013-04-08'),
(685, 182, '2013-04-09'),
(686, 182, '2013-04-10'),
(687, 182, '2013-04-11'),
(688, 182, '2013-04-12'),
(689, 182, '2013-04-13'),
(690, 182, '2013-04-14'),
(691, 182, '2013-04-15'),
(692, 182, '2013-04-16'),
(693, 182, '2013-04-17'),
(694, 182, '2013-04-18'),
(695, 182, '2013-04-19'),
(696, 182, '2013-04-20'),
(697, 182, '2013-04-21'),
(698, 182, '2013-04-22'),
(699, 182, '2013-04-23'),
(700, 182, '2013-04-24'),
(701, 182, '2013-04-25'),
(702, 182, '2013-04-26'),
(703, 182, '2013-04-27'),
(704, 182, '2013-04-28'),
(705, 182, '2013-04-29'),
(706, 182, '2013-04-30'),
(707, 182, '2018-12-24'),
(708, 182, '2018-12-26'),
(710, 182, '2019-02-03'),
(711, 182, '2019-02-04'),
(712, 182, '2019-01-02'),
(713, 183, '2019-02-04'),
(717, 183, '2019-01-04'),
(723, 183, '2019-01-06'),
(728, 183, '2019-01-31'),
(729, 183, '2019-01-30'),
(730, 183, '2019-01-29'),
(731, 183, '2019-01-23'),
(732, 183, '2019-01-15'),
(733, 183, '2019-01-20'),
(734, 183, '2019-01-16'),
(735, 183, '2019-01-18'),
(736, 183, '2019-02-28'),
(743, 183, '2019-02-04'),
(745, 183, '2019-01-04'),
(746, 183, '2018-11-08'),
(749, 183, '2019-02-04'),
(751, 183, '2019-02-04'),
(763, 184, '2019-01-05'),
(764, 184, '2019-01-05'),
(765, 184, '2019-01-05'),
(766, 184, '2019-01-05'),
(767, 184, '2019-01-04'),
(768, 184, '2019-01-04'),
(772, 183, '2018-12-05'),
(773, 183, '2019-01-05'),
(775, 183, '2019-01-04'),
(776, 183, '2019-01-09'),
(777, 183, '2019-01-09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(2) NOT NULL,
  `kelas` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `kelas`) VALUES
(1, '1A'),
(2, '1B'),
(3, '2A'),
(4, '2B'),
(5, '3A'),
(6, '3B'),
(7, '4A'),
(8, '4B'),
(9, '5A'),
(10, '5B'),
(11, '6A'),
(12, '6B');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_mengaji`
--

CREATE TABLE `tb_mengaji` (
  `id_mengaji` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `bacaanAwal` varchar(30) DEFAULT NULL,
  `bacaanAkhir` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_mengaji`
--

INSERT INTO `tb_mengaji` (`id_mengaji`, `id_kegiatan`, `bacaanAwal`, `bacaanAkhir`) VALUES
(395, 401, '1', '11'),
(396, 402, '2', '22'),
(397, 403, '3', '33'),
(398, 404, '4', '44'),
(399, 405, '5', '55'),
(400, 406, '11', ''),
(401, 407, '22', ''),
(402, 408, '33', ''),
(403, 409, '44', ''),
(404, 410, '', ''),
(405, 411, '', ''),
(406, 412, '', ''),
(407, 413, '', ''),
(408, 414, '', ''),
(409, 415, '', ''),
(410, 416, '', ''),
(411, 417, '', ''),
(412, 418, '', ''),
(413, 419, '', ''),
(414, 420, '', ''),
(415, 421, '', ''),
(416, 422, '', ''),
(417, 423, '', ''),
(418, 424, '', ''),
(419, 425, '', ''),
(420, 426, '', ''),
(421, 427, '', ''),
(422, 428, '', ''),
(423, 429, '', ''),
(424, 430, '', ''),
(425, 431, '', ''),
(426, 441, '', ''),
(427, 442, '', ''),
(428, 443, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(429, 444, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(430, 445, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(431, 446, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(432, 454, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(433, 455, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(434, 456, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(435, 457, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(438, 460, 'al- baqarah -', '100 ayat'),
(439, 461, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(440, 462, '', ''),
(441, 463, '', ''),
(442, 464, '', ''),
(443, 465, '', ''),
(444, 466, '', ''),
(445, 467, '', ''),
(446, 468, '', ''),
(447, 469, '', ''),
(448, 470, '', ''),
(449, 471, '', ''),
(450, 472, '', ''),
(451, 473, '', ''),
(452, 474, '', ''),
(453, 475, '', ''),
(454, 476, '', ''),
(455, 477, '', ''),
(456, 478, 'al - baqarah', 'ayat 100'),
(457, 479, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(458, 480, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(459, 481, '', ''),
(460, 482, '', ''),
(461, 483, 'al - baqarah', 'ayat 100'),
(462, 484, '', ''),
(463, 485, '', ''),
(464, 486, '', ''),
(465, 487, '', ''),
(466, 488, '11', '12'),
(467, 489, '21', '22'),
(468, 490, '31', '33'),
(469, 491, '41', '44'),
(470, 492, '51', '55'),
(471, 493, '', ''),
(472, 494, '', ''),
(473, 495, '', ''),
(474, 496, '', ''),
(475, 497, '', ''),
(476, 498, '', ''),
(477, 499, '', ''),
(478, 500, '', ''),
(479, 501, '', ''),
(480, 502, '', ''),
(481, 503, '', ''),
(482, 504, '', ''),
(483, 505, '', ''),
(484, 506, '', ''),
(485, 507, '', ''),
(486, 508, '', ''),
(487, 509, '', ''),
(488, 510, '', ''),
(489, 511, '', ''),
(490, 512, '', ''),
(491, 513, '', ''),
(492, 514, '', ''),
(493, 515, '', ''),
(494, 516, '', ''),
(495, 517, '', ''),
(496, 518, '', ''),
(497, 519, '', ''),
(498, 520, '', ''),
(499, 521, '', ''),
(500, 522, '', ''),
(501, 523, '', ''),
(502, 524, '', ''),
(503, 525, '', ''),
(504, 526, '', ''),
(505, 527, '', ''),
(506, 528, '', ''),
(507, 529, '', ''),
(508, 530, '', ''),
(509, 531, '', ''),
(510, 532, '', ''),
(511, 533, '', ''),
(512, 534, '', ''),
(513, 535, '', ''),
(514, 536, '', ''),
(515, 537, '', ''),
(516, 538, '', ''),
(517, 539, '', ''),
(518, 540, '', ''),
(519, 541, '', ''),
(520, 542, '', ''),
(521, 543, '', ''),
(522, 544, '', ''),
(523, 545, '', ''),
(524, 546, '', ''),
(525, 547, '', ''),
(526, 548, '', ''),
(527, 549, '', ''),
(528, 550, '', ''),
(529, 551, 'al - baqarah', 'ayat 100'),
(530, 552, '', ''),
(531, 553, '1', '1'),
(532, 554, '1', '1'),
(533, 555, '2', '2'),
(534, 556, '2', '2'),
(535, 557, '3', '3'),
(536, 558, '4', '4'),
(537, 559, '5', '5'),
(538, 560, '', ''),
(539, 561, '', ''),
(540, 562, '', ''),
(541, 563, '', ''),
(542, 564, '', ''),
(543, 565, '', ''),
(544, 566, '', ''),
(545, 567, '', ''),
(546, 568, '', ''),
(547, 569, '', ''),
(548, 570, '', ''),
(549, 571, '', ''),
(550, 572, '', ''),
(551, 573, '', ''),
(552, 574, '', ''),
(553, 575, '', ''),
(554, 576, '', ''),
(555, 577, '', ''),
(556, 578, '', ''),
(557, 579, '', ''),
(558, 580, '', ''),
(559, 581, '', ''),
(560, 582, '', ''),
(561, 583, '1', '1'),
(562, 584, '1', '1'),
(563, 585, '1', '1'),
(564, 586, '1', '1'),
(565, 587, '1', '1'),
(566, 588, '', ''),
(567, 589, '', ''),
(568, 590, '1', '1'),
(569, 591, '', ''),
(570, 592, '', ''),
(571, 593, '1', '1'),
(572, 594, '', ''),
(573, 595, '1', '1'),
(574, 596, '', ''),
(575, 597, '1', '1'),
(576, 598, '1', '1'),
(577, 599, '1', '1'),
(578, 600, '', ''),
(579, 601, '1', '1'),
(580, 602, '1', '1'),
(581, 603, '1', '1'),
(582, 604, '1', '1'),
(583, 605, '', ''),
(584, 606, '1', '1'),
(585, 607, '', ''),
(586, 608, '1', '1'),
(587, 609, '1', '1'),
(588, 610, '1', '1'),
(589, 611, '1', '1'),
(590, 612, '1', '1'),
(591, 613, '', ''),
(592, 614, '', ''),
(593, 615, '', ''),
(594, 616, '', ''),
(595, 617, '', ''),
(596, 618, '', ''),
(597, 619, '', ''),
(598, 620, '', ''),
(599, 621, '', ''),
(600, 622, '', ''),
(601, 623, '', ''),
(602, 624, '', ''),
(603, 625, '', ''),
(604, 626, '', ''),
(605, 627, '', ''),
(606, 628, '', ''),
(607, 629, '', ''),
(608, 630, '', ''),
(609, 631, '', ''),
(610, 632, '', ''),
(611, 633, '', ''),
(612, 634, '', ''),
(613, 635, '', ''),
(614, 636, '', ''),
(615, 637, '', ''),
(616, 638, '', ''),
(617, 639, '', ''),
(618, 640, '', ''),
(619, 641, '', ''),
(620, 642, '', ''),
(621, 643, '', ''),
(622, 644, '', ''),
(623, 645, '', ''),
(624, 646, '', ''),
(625, 647, '', ''),
(626, 648, '', ''),
(627, 649, '', ''),
(628, 650, '', ''),
(629, 651, '', ''),
(630, 652, '', ''),
(631, 653, '', ''),
(632, 654, '', ''),
(633, 655, '', ''),
(634, 656, '', ''),
(635, 657, '', ''),
(636, 658, '', ''),
(637, 659, '', ''),
(638, 660, '', ''),
(639, 661, '', ''),
(640, 662, '', ''),
(641, 663, '', ''),
(642, 664, '', ''),
(643, 665, '', ''),
(644, 666, '', ''),
(645, 667, '', ''),
(646, 668, '', ''),
(647, 669, '', ''),
(648, 670, '', ''),
(649, 671, '', ''),
(650, 672, '', ''),
(651, 673, '', ''),
(652, 677, '1', '1'),
(653, 678, '', ''),
(654, 679, '', ''),
(655, 680, '', ''),
(656, 681, '', ''),
(657, 682, '', ''),
(658, 683, '', ''),
(659, 684, '', ''),
(660, 685, '', ''),
(661, 686, '', ''),
(662, 687, '', ''),
(663, 688, '', ''),
(664, 689, '', ''),
(665, 690, '', ''),
(666, 691, '', ''),
(667, 692, '', ''),
(668, 693, '', ''),
(669, 694, '', ''),
(670, 695, '', ''),
(671, 696, '', ''),
(672, 697, '', ''),
(673, 698, '', ''),
(674, 699, '', ''),
(675, 700, '', ''),
(676, 701, '', ''),
(677, 702, '', ''),
(678, 703, '', ''),
(679, 704, '', ''),
(680, 705, '', ''),
(681, 706, '', ''),
(682, 707, 'Yasin - 01', 'Yasin - 20'),
(683, 708, '', ''),
(685, 710, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(686, 711, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(687, 712, 'Al baqarah ayat 1', 'Albaqarah ayat 100'),
(688, 713, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(692, 717, 'Al baqarah ayat 101', 'Al baqarah ayat 201'),
(698, 723, 'al- baqarah - ayat 2', '100 ayat al'),
(703, 728, 'Al-ikhlas ayat 2', 'Al-ikhlas ayat 3'),
(704, 729, 'Al-ikhlas ayat 1', 'Al-ikhlas ayat 1'),
(705, 730, '1', '12'),
(706, 731, '7', '6'),
(707, 732, 'Qw', 'Erty'),
(708, 733, '10', '90'),
(709, 734, '12', '40'),
(710, 735, '123', '321'),
(711, 736, '', ''),
(718, 743, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(720, 745, '', ''),
(721, 746, '', ''),
(724, 749, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(726, 751, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(738, 763, '', ''),
(739, 764, '', ''),
(740, 765, '', ''),
(741, 766, '', ''),
(742, 767, '', ''),
(743, 768, '', ''),
(747, 772, '', ''),
(748, 773, 'al-baqarah ayat 1', 'al-baqarah ayat 100'),
(750, 775, '', '123'),
(751, 776, '', ''),
(752, 777, 'Al baqarah ayat1', 'Al baqarah ayat 50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_salat`
--

CREATE TABLE `tb_salat` (
  `id_solat` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `s_subuh` tinyint(1) DEFAULT NULL,
  `s_dzuhur` tinyint(1) DEFAULT NULL,
  `s_ashar` tinyint(1) DEFAULT NULL,
  `s_maghrib` tinyint(1) DEFAULT NULL,
  `s_isya` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tb_salat`
--

INSERT INTO `tb_salat` (`id_solat`, `id_kegiatan`, `s_subuh`, `s_dzuhur`, `s_ashar`, `s_maghrib`, `s_isya`) VALUES
(395, 401, NULL, NULL, NULL, 0, NULL),
(396, 402, NULL, 0, NULL, NULL, NULL),
(397, 403, NULL, 0, 0, 0, NULL),
(398, 404, NULL, NULL, NULL, 0, NULL),
(399, 405, NULL, NULL, 0, 0, 0),
(400, 406, 0, NULL, NULL, NULL, NULL),
(401, 407, NULL, 0, NULL, NULL, NULL),
(402, 408, NULL, NULL, 0, NULL, NULL),
(403, 409, NULL, 0, NULL, NULL, NULL),
(404, 410, 0, NULL, NULL, NULL, NULL),
(405, 411, NULL, NULL, NULL, NULL, NULL),
(406, 412, NULL, NULL, NULL, NULL, NULL),
(407, 413, NULL, NULL, NULL, NULL, NULL),
(408, 414, NULL, NULL, NULL, NULL, NULL),
(409, 415, NULL, NULL, NULL, NULL, NULL),
(410, 416, NULL, NULL, NULL, NULL, NULL),
(411, 417, NULL, NULL, NULL, NULL, NULL),
(412, 418, NULL, NULL, NULL, NULL, NULL),
(413, 419, NULL, NULL, NULL, NULL, NULL),
(414, 420, NULL, NULL, NULL, NULL, NULL),
(415, 421, NULL, NULL, NULL, NULL, NULL),
(416, 422, NULL, NULL, NULL, NULL, NULL),
(417, 423, NULL, NULL, NULL, NULL, NULL),
(418, 424, NULL, NULL, NULL, NULL, NULL),
(419, 425, NULL, NULL, NULL, NULL, NULL),
(420, 426, NULL, NULL, NULL, NULL, NULL),
(421, 427, NULL, NULL, NULL, NULL, NULL),
(422, 428, NULL, NULL, NULL, NULL, NULL),
(423, 429, NULL, NULL, NULL, NULL, NULL),
(424, 430, NULL, NULL, NULL, NULL, NULL),
(425, 431, NULL, NULL, NULL, NULL, NULL),
(426, 441, NULL, NULL, NULL, NULL, NULL),
(427, 442, 0, 0, 0, 0, 0),
(428, 443, NULL, NULL, NULL, NULL, NULL),
(429, 444, NULL, NULL, NULL, NULL, NULL),
(430, 445, NULL, NULL, NULL, NULL, NULL),
(431, 446, NULL, NULL, NULL, NULL, NULL),
(433, 454, NULL, NULL, NULL, NULL, NULL),
(434, 455, NULL, NULL, NULL, NULL, NULL),
(435, 456, NULL, NULL, NULL, NULL, NULL),
(436, 457, NULL, NULL, NULL, NULL, NULL),
(439, 460, NULL, NULL, NULL, NULL, NULL),
(440, 461, NULL, NULL, NULL, NULL, NULL),
(441, 462, 0, NULL, NULL, NULL, NULL),
(442, 463, NULL, NULL, NULL, NULL, NULL),
(443, 464, NULL, NULL, NULL, NULL, NULL),
(444, 465, NULL, NULL, NULL, NULL, NULL),
(445, 466, NULL, NULL, NULL, NULL, NULL),
(446, 467, NULL, NULL, NULL, NULL, NULL),
(447, 468, NULL, NULL, NULL, NULL, NULL),
(448, 469, NULL, NULL, NULL, NULL, NULL),
(449, 470, NULL, NULL, NULL, NULL, NULL),
(450, 471, NULL, NULL, NULL, NULL, NULL),
(451, 472, NULL, NULL, NULL, NULL, NULL),
(452, 473, NULL, NULL, NULL, NULL, NULL),
(453, 474, NULL, NULL, NULL, NULL, NULL),
(454, 475, NULL, NULL, NULL, NULL, NULL),
(455, 476, NULL, NULL, NULL, NULL, NULL),
(456, 477, NULL, NULL, NULL, NULL, NULL),
(457, 478, 0, 0, 0, 0, 0),
(458, 479, NULL, NULL, NULL, NULL, NULL),
(459, 480, NULL, NULL, NULL, NULL, NULL),
(460, 481, NULL, NULL, NULL, NULL, NULL),
(461, 482, NULL, NULL, NULL, NULL, NULL),
(462, 483, 0, 0, 0, 0, 0),
(463, 484, NULL, NULL, NULL, NULL, NULL),
(464, 485, NULL, NULL, NULL, NULL, NULL),
(465, 486, NULL, NULL, NULL, NULL, NULL),
(466, 487, NULL, NULL, NULL, NULL, NULL),
(467, 488, 0, NULL, NULL, NULL, NULL),
(468, 489, NULL, 0, NULL, NULL, NULL),
(469, 490, NULL, NULL, 0, NULL, NULL),
(470, 491, NULL, NULL, NULL, 0, NULL),
(471, 492, NULL, NULL, NULL, NULL, 0),
(472, 493, NULL, NULL, NULL, NULL, NULL),
(473, 494, NULL, NULL, NULL, NULL, NULL),
(474, 495, NULL, NULL, NULL, NULL, NULL),
(475, 496, NULL, NULL, NULL, NULL, NULL),
(476, 497, NULL, NULL, NULL, NULL, NULL),
(477, 498, NULL, NULL, NULL, NULL, NULL),
(478, 499, NULL, NULL, NULL, NULL, NULL),
(479, 500, NULL, NULL, NULL, NULL, NULL),
(480, 501, NULL, NULL, NULL, NULL, NULL),
(481, 502, NULL, NULL, NULL, NULL, NULL),
(482, 503, NULL, NULL, NULL, NULL, NULL),
(483, 504, NULL, NULL, NULL, NULL, NULL),
(484, 505, NULL, NULL, NULL, NULL, NULL),
(485, 506, NULL, NULL, NULL, NULL, NULL),
(486, 507, NULL, NULL, NULL, NULL, NULL),
(487, 508, NULL, NULL, NULL, NULL, NULL),
(488, 509, NULL, NULL, NULL, NULL, NULL),
(489, 510, NULL, NULL, NULL, NULL, NULL),
(490, 511, NULL, NULL, NULL, NULL, NULL),
(491, 512, NULL, NULL, NULL, NULL, NULL),
(492, 513, NULL, NULL, NULL, NULL, NULL),
(493, 514, NULL, NULL, NULL, NULL, NULL),
(494, 515, NULL, NULL, NULL, NULL, NULL),
(495, 516, NULL, NULL, NULL, NULL, NULL),
(496, 517, NULL, NULL, NULL, NULL, NULL),
(497, 518, NULL, NULL, NULL, NULL, NULL),
(498, 519, NULL, NULL, NULL, NULL, NULL),
(499, 520, NULL, NULL, NULL, NULL, NULL),
(500, 521, NULL, NULL, NULL, NULL, NULL),
(501, 522, 0, NULL, NULL, NULL, NULL),
(502, 523, NULL, 0, NULL, NULL, NULL),
(503, 524, NULL, NULL, 0, NULL, NULL),
(504, 525, NULL, NULL, NULL, NULL, NULL),
(505, 526, NULL, NULL, NULL, NULL, NULL),
(506, 527, NULL, NULL, NULL, NULL, NULL),
(507, 528, NULL, NULL, NULL, NULL, NULL),
(508, 529, NULL, NULL, NULL, NULL, NULL),
(509, 530, NULL, NULL, NULL, NULL, NULL),
(510, 531, NULL, NULL, NULL, NULL, NULL),
(511, 532, NULL, NULL, NULL, NULL, NULL),
(512, 533, NULL, NULL, NULL, NULL, NULL),
(513, 534, NULL, NULL, NULL, NULL, NULL),
(514, 535, NULL, NULL, NULL, NULL, NULL),
(515, 536, NULL, NULL, NULL, NULL, NULL),
(516, 537, NULL, NULL, NULL, NULL, NULL),
(517, 538, NULL, NULL, NULL, NULL, NULL),
(518, 539, NULL, NULL, NULL, NULL, NULL),
(519, 540, NULL, NULL, NULL, NULL, NULL),
(520, 541, NULL, NULL, NULL, NULL, NULL),
(521, 542, NULL, NULL, NULL, NULL, NULL),
(522, 543, NULL, NULL, NULL, NULL, NULL),
(523, 544, NULL, NULL, NULL, NULL, NULL),
(524, 545, NULL, NULL, NULL, NULL, NULL),
(525, 546, NULL, NULL, NULL, NULL, NULL),
(526, 547, NULL, NULL, NULL, NULL, NULL),
(527, 548, NULL, NULL, NULL, NULL, NULL),
(528, 549, NULL, NULL, NULL, NULL, NULL),
(529, 550, NULL, NULL, NULL, NULL, NULL),
(530, 551, 0, 0, 0, 0, 0),
(531, 552, NULL, NULL, NULL, NULL, NULL),
(532, 553, 0, NULL, NULL, NULL, NULL),
(533, 554, NULL, 0, NULL, NULL, NULL),
(534, 555, NULL, NULL, 0, NULL, NULL),
(535, 556, NULL, NULL, NULL, 0, NULL),
(536, 557, NULL, NULL, NULL, NULL, 0),
(537, 558, NULL, NULL, NULL, 0, NULL),
(538, 559, NULL, NULL, 0, NULL, NULL),
(539, 560, NULL, NULL, 0, NULL, NULL),
(540, 561, NULL, NULL, 0, NULL, NULL),
(541, 562, NULL, 0, NULL, NULL, NULL),
(542, 563, NULL, 0, NULL, NULL, NULL),
(543, 564, 0, NULL, NULL, NULL, NULL),
(544, 565, NULL, 0, NULL, NULL, NULL),
(545, 566, NULL, 0, NULL, NULL, NULL),
(546, 567, NULL, NULL, 0, NULL, NULL),
(547, 568, NULL, NULL, NULL, 0, NULL),
(548, 569, NULL, NULL, NULL, NULL, 0),
(549, 570, NULL, NULL, NULL, 0, NULL),
(550, 571, NULL, NULL, 0, NULL, NULL),
(551, 572, NULL, 0, NULL, NULL, NULL),
(552, 573, NULL, 0, NULL, NULL, NULL),
(553, 574, NULL, 0, NULL, NULL, NULL),
(554, 575, 0, NULL, NULL, NULL, NULL),
(555, 576, NULL, 0, NULL, NULL, NULL),
(556, 577, NULL, 0, NULL, NULL, NULL),
(557, 578, 0, NULL, NULL, NULL, NULL),
(558, 579, 0, NULL, NULL, NULL, NULL),
(559, 580, 0, NULL, NULL, NULL, NULL),
(560, 581, NULL, 0, NULL, NULL, NULL),
(561, 582, NULL, 0, NULL, NULL, NULL),
(562, 583, 0, 0, 0, 0, 0),
(563, 584, 0, 0, 0, 0, 0),
(564, 585, 0, 0, 0, 0, 0),
(565, 586, 0, 0, 0, 0, 0),
(566, 587, 0, 0, 0, 0, 0),
(567, 588, 0, 0, 0, 0, 0),
(568, 589, 0, 0, 0, 0, 0),
(569, 590, 0, 0, 0, 0, 0),
(570, 591, 0, 0, 0, 0, 0),
(571, 592, 0, 0, 0, 0, 0),
(572, 593, 0, 0, 0, 0, 0),
(573, 594, NULL, NULL, 0, NULL, 0),
(574, 595, NULL, NULL, 0, 0, 0),
(575, 596, NULL, 0, 0, 0, 0),
(576, 597, NULL, 0, 0, NULL, NULL),
(577, 598, NULL, 0, 0, NULL, NULL),
(578, 599, NULL, 0, 0, 0, NULL),
(579, 600, NULL, 0, 0, 0, NULL),
(580, 601, 0, 0, NULL, 0, 0),
(581, 602, 0, NULL, 0, NULL, NULL),
(582, 603, 0, 0, 0, 0, NULL),
(583, 604, 0, 0, NULL, 0, NULL),
(584, 605, NULL, NULL, NULL, 0, 0),
(585, 606, NULL, 0, 0, 0, 0),
(586, 607, NULL, 0, NULL, NULL, NULL),
(587, 608, 0, 0, NULL, 0, 0),
(588, 609, 0, NULL, 0, 0, 0),
(589, 610, 0, NULL, 0, 0, NULL),
(590, 611, 0, NULL, 0, 0, 0),
(591, 612, 0, 0, 0, NULL, 0),
(592, 613, 0, NULL, NULL, NULL, NULL),
(593, 614, NULL, NULL, NULL, NULL, NULL),
(594, 615, NULL, NULL, NULL, NULL, NULL),
(595, 616, NULL, NULL, NULL, NULL, NULL),
(596, 617, NULL, NULL, NULL, NULL, NULL),
(597, 618, NULL, NULL, NULL, NULL, NULL),
(598, 619, NULL, NULL, NULL, NULL, NULL),
(599, 620, NULL, NULL, NULL, NULL, NULL),
(600, 621, NULL, NULL, NULL, NULL, NULL),
(601, 622, NULL, NULL, NULL, NULL, NULL),
(602, 623, NULL, NULL, NULL, NULL, NULL),
(603, 624, NULL, NULL, NULL, NULL, NULL),
(604, 625, NULL, NULL, NULL, NULL, NULL),
(605, 626, NULL, NULL, NULL, NULL, NULL),
(606, 627, NULL, NULL, NULL, NULL, NULL),
(607, 628, NULL, NULL, NULL, NULL, NULL),
(608, 629, NULL, NULL, NULL, NULL, NULL),
(609, 630, NULL, NULL, NULL, NULL, NULL),
(610, 631, NULL, NULL, NULL, NULL, NULL),
(611, 632, NULL, NULL, NULL, NULL, NULL),
(612, 633, NULL, NULL, NULL, NULL, NULL),
(613, 634, NULL, NULL, NULL, NULL, NULL),
(614, 635, NULL, NULL, NULL, NULL, NULL),
(615, 636, NULL, NULL, NULL, NULL, NULL),
(616, 637, NULL, NULL, NULL, NULL, NULL),
(617, 638, NULL, NULL, NULL, NULL, NULL),
(618, 639, NULL, NULL, NULL, NULL, NULL),
(619, 640, NULL, NULL, NULL, NULL, NULL),
(620, 641, NULL, NULL, NULL, NULL, NULL),
(621, 642, NULL, NULL, NULL, NULL, NULL),
(622, 643, 0, NULL, NULL, NULL, NULL),
(623, 644, NULL, 0, NULL, NULL, NULL),
(624, 645, NULL, NULL, 0, NULL, NULL),
(625, 646, NULL, NULL, NULL, 0, NULL),
(626, 647, NULL, NULL, NULL, NULL, 0),
(627, 648, NULL, NULL, NULL, 0, NULL),
(628, 649, NULL, NULL, 0, NULL, NULL),
(629, 650, NULL, 0, NULL, NULL, NULL),
(630, 651, 0, NULL, NULL, NULL, NULL),
(631, 652, NULL, 0, NULL, NULL, NULL),
(632, 653, NULL, NULL, 0, NULL, NULL),
(633, 654, NULL, NULL, NULL, 0, NULL),
(634, 655, NULL, NULL, NULL, NULL, 0),
(635, 656, NULL, NULL, NULL, 0, 0),
(636, 657, NULL, NULL, 0, 0, NULL),
(637, 658, NULL, 0, 0, NULL, NULL),
(638, 659, 0, 0, NULL, NULL, NULL),
(639, 660, 0, 0, 0, NULL, NULL),
(640, 661, NULL, 0, 0, 0, NULL),
(641, 662, NULL, NULL, 0, 0, 0),
(642, 663, 0, NULL, 0, NULL, 0),
(643, 664, 0, NULL, 0, 0, NULL),
(644, 665, 0, NULL, 0, NULL, 0),
(645, 666, NULL, 0, NULL, 0, NULL),
(646, 667, 0, 0, NULL, NULL, NULL),
(647, 668, 0, 0, NULL, NULL, NULL),
(648, 669, NULL, NULL, 0, 0, NULL),
(649, 670, NULL, NULL, 0, 0, NULL),
(650, 671, NULL, NULL, NULL, 0, 0),
(651, 672, NULL, NULL, NULL, 0, 0),
(652, 673, NULL, NULL, 0, 0, NULL),
(653, 612, 0, 0, 0, 0, 0),
(654, 676, 0, 0, 0, 0, 0),
(655, 676, 0, 0, 0, 0, 0),
(656, 677, 0, 0, NULL, NULL, NULL),
(657, 678, 0, NULL, NULL, NULL, NULL),
(658, 679, 0, NULL, NULL, NULL, NULL),
(659, 680, 0, NULL, NULL, NULL, NULL),
(660, 681, 0, NULL, NULL, NULL, NULL),
(661, 682, 0, NULL, NULL, NULL, NULL),
(662, 683, 0, NULL, NULL, NULL, NULL),
(663, 684, 0, NULL, NULL, NULL, NULL),
(664, 685, 0, NULL, NULL, NULL, NULL),
(665, 686, 0, NULL, NULL, NULL, NULL),
(666, 687, 0, NULL, NULL, NULL, NULL),
(667, 688, 0, NULL, NULL, NULL, NULL),
(668, 689, 0, NULL, NULL, NULL, NULL),
(669, 690, 0, NULL, NULL, NULL, NULL),
(670, 691, 0, NULL, NULL, NULL, NULL),
(671, 692, 0, NULL, NULL, NULL, NULL),
(672, 693, 0, NULL, NULL, NULL, NULL),
(673, 694, 0, NULL, NULL, NULL, NULL),
(674, 695, 0, NULL, NULL, NULL, NULL),
(675, 696, 0, NULL, NULL, NULL, NULL),
(676, 697, 0, NULL, NULL, NULL, NULL),
(677, 698, 0, NULL, NULL, NULL, NULL),
(678, 699, 0, NULL, NULL, NULL, NULL),
(679, 700, 0, NULL, NULL, NULL, NULL),
(680, 701, 0, NULL, NULL, NULL, NULL),
(681, 702, 0, NULL, NULL, NULL, NULL),
(682, 703, 0, NULL, NULL, NULL, NULL),
(683, 704, 0, NULL, NULL, NULL, NULL),
(684, 705, 0, NULL, NULL, NULL, NULL),
(685, 706, 0, NULL, NULL, NULL, NULL),
(686, 707, 1, 1, 0, 0, 0),
(687, 708, 0, 0, 0, 0, 0),
(689, 710, 0, NULL, 0, NULL, 0),
(690, 711, 0, NULL, 0, NULL, 0),
(691, 712, 1, 1, 1, 1, 0),
(692, 713, 0, NULL, 0, NULL, 0),
(696, 717, 0, 1, 1, 1, 1),
(702, 723, 0, 0, 0, 0, 0),
(707, 728, 0, 0, 0, 0, 0),
(708, 729, 0, 0, 0, 0, 0),
(709, 730, 0, 0, 0, 0, 0),
(710, 731, 0, 0, 0, 0, 0),
(711, 732, 0, 0, 0, 0, 0),
(712, 733, 0, 0, NULL, 0, 0),
(713, 734, 0, 0, 0, 0, 0),
(714, 735, NULL, 0, 0, 0, 0),
(715, 736, NULL, NULL, NULL, NULL, NULL),
(722, 743, NULL, NULL, NULL, NULL, NULL),
(724, 745, NULL, NULL, NULL, NULL, NULL),
(725, 746, NULL, NULL, NULL, NULL, NULL),
(728, 749, NULL, NULL, NULL, NULL, NULL),
(730, 751, NULL, NULL, NULL, NULL, NULL),
(742, 763, NULL, NULL, NULL, NULL, NULL),
(743, 764, NULL, NULL, NULL, NULL, NULL),
(744, 765, NULL, NULL, NULL, NULL, NULL),
(745, 766, NULL, NULL, NULL, NULL, NULL),
(746, 767, NULL, NULL, NULL, NULL, NULL),
(747, 768, NULL, NULL, NULL, NULL, NULL),
(751, 772, 0, 0, NULL, 0, NULL),
(752, 773, 0, NULL, 0, NULL, 0),
(754, 775, 0, NULL, 0, NULL, 0),
(756, 775, 0, NULL, 0, NULL, 0),
(757, 775, 0, NULL, 0, NULL, 0),
(758, 776, NULL, NULL, NULL, NULL, NULL),
(759, 777, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nis_lokal` varchar(20) NOT NULL,
  `nisn` varchar(16) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `ttl` varchar(60) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `agama` varchar(8) NOT NULL,
  `id_kelas` int(2) NOT NULL,
  `foto` varchar(60) DEFAULT 'no_photo.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_siswa`
--

INSERT INTO `tb_siswa` (`id_siswa`, `id_user`, `nis_lokal`, `nisn`, `nik`, `nama`, `ttl`, `alamat`, `jenis_kelamin`, `agama`, `id_kelas`, `foto`) VALUES
(182, 255, '123456', '123456', '123456', 'helmi mutawalli', 'Bandung, 23 MEi 2019', 'Jl.Gegerkalong no 54', 'L', 'islam', 1, 'no_photo'),
(183, 257, '1234567891', '01987654322', '123123456457', 'kiki hadiansah', 'bandung, 30 desember 1995', 'jl. kuningan', '', '', 1, 'no_photo.jpg'),
(184, 258, '12345678912', '01987654323', '123123456458', 'haidir mus', 'malang, 22 Agustus 1996', 'antapani', '', '', 1, 'no_photo'),
(185, 259, '123456789', '123456789', '1231234564567', 'jihad al qudsi', 'cikarang, 8 november 1997', 'rajawali', '', '', 1, 'no_photo'),
(186, 260, '123456', '123456', '123456', 'jajang', 'bandung', 'a yani', '', '', 1, 'no_photo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` varchar(25) NOT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `email`, `level`, `token`) VALUES
(1, 'admin', '1234', 'admin@gmail.com', 'admin', ''),
(255, 'helmi', '1234', '', 'siswa', 'c3f63eNhSoM:APA91bE3tWSuU2MsGz-tuJioukBRWsSrXLEckv1fM9sdF46YcJsnl8OOMy-lVwqPEbLCORtTn1dk1n5yriMUa510ArxXYFlMwdtP-nUJa0vwZgWz6b02jUD49lpRDXNiay-vQdg1SRcP'),
(256, 'dinar', '1234', '', 'guru', ''),
(257, 'kiki', '1234', '', 'siswa', ''),
(258, 'haidir', '1234', '', 'siswa', ''),
(259, 'jihad', '1234', '', 'siswa', ''),
(260, 'jajang', '1234', '', 'siswa', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`),
  ADD KEY `foto` (`foto`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id_foto`),
  ADD KEY `nama` (`nama`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id_galeri`);

--
-- Indexes for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`),
  ADD KEY `foto` (`foto`);

--
-- Indexes for table `tb_catatan`
--
ALTER TABLE `tb_catatan`
  ADD PRIMARY KEY (`id_catatan`),
  ADD KEY `id_kegiatan` (`id_kegiatan`);

--
-- Indexes for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `kelas` (`kelas`);

--
-- Indexes for table `tb_mengaji`
--
ALTER TABLE `tb_mengaji`
  ADD PRIMARY KEY (`id_mengaji`),
  ADD KEY `id_kegiatan` (`id_kegiatan`);

--
-- Indexes for table `tb_salat`
--
ALTER TABLE `tb_salat`
  ADD PRIMARY KEY (`id_solat`),
  ADD KEY `id_kegiatan` (`id_kegiatan`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id_galeri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_catatan`
--
ALTER TABLE `tb_catatan`
  MODIFY `id_catatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `tb_guru`
--
ALTER TABLE `tb_guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=778;
--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_mengaji`
--
ALTER TABLE `tb_mengaji`
  MODIFY `id_mengaji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=753;
--
-- AUTO_INCREMENT for table `tb_salat`
--
ALTER TABLE `tb_salat`
  MODIFY `id_solat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=760;
--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `berita_ibfk_1` FOREIGN KEY (`foto`) REFERENCES `foto` (`nama`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`foto`) REFERENCES `foto` (`nama`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_catatan`
--
ALTER TABLE `tb_catatan`
  ADD CONSTRAINT `tb_catatan_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `tb_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD CONSTRAINT `tb_guru_ibfk_4` FOREIGN KEY (`id_kelas`) REFERENCES `tb_kelas` (`id_kelas`),
  ADD CONSTRAINT `tb_guru_ibfk_5` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD CONSTRAINT `tb_kegiatan_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `tb_siswa` (`id_siswa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_mengaji`
--
ALTER TABLE `tb_mengaji`
  ADD CONSTRAINT `tb_mengaji_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `tb_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_salat`
--
ALTER TABLE `tb_salat`
  ADD CONSTRAINT `tb_salat_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `tb_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD CONSTRAINT `tb_siswa_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `tb_kelas` (`id_kelas`),
  ADD CONSTRAINT `tb_siswa_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
