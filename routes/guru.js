var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'db_mi_cahaya'
});
var global_json_data;
//  Sebagai Index page
router.get('/',function (req,res) {
    var data = {
        "Data":""
    };
    data["Data"] = "MI Cahaya Web Service";
    res.json(data);
});


//  Mengambil Data guru
router.get('/(:id_user)',function (req,res) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query("SELECT tb_guru.nama,tb_kelas.kelas FROM tb_user JOIN tb_guru JOIN tb_kelas ON tb_user.id_user = tb_guru.id_user && tb_kelas.id_kelas = tb_guru.id_kelas WHERE tb_user.id_user = "+req.params.id_user,function (err,rows,fields) {
        if (rows.length !=0){
            res.json(rows);
        }else{
            data["Data Siswa"]='Tidak ada data siswa...';
            res.json(data);
        }
    })
});

// Detail data Guru
router.get('/tampilDetailGuru/(:id_user_siswa)',function (req,res) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query("SELECT tb_guru.nip,tb_guru.nuptk,tb_guru.nik,tb_guru.nama,tb_guru.ttl,tb_guru.jenis_kelamin,tb_kelas.kelas,tb_guru.alamat,tb_guru.agama FROM tb_guru JOIN tb_kelas ON tb_guru.id_kelas = tb_kelas.id_kelas JOIN tb_siswa ON tb_kelas.id_kelas = tb_siswa.id_kelas WHERE tb_siswa.id_user = "+req.params.id_user_siswa,function (err,rows,fields) {
        if (rows.length !=0){
            res.json(rows);
        }else{
            data["Data Siswa"]='Tidak ada data siswa...';
            res.json(data);
        }
    })
});


module.exports = router;