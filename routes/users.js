var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'db_mi_cahaya'
});
var global_json_data;
//  Sebagai Index page
router.get('/',function (req,res) {
    var data = {
        "Data":""
    };
    data["Data"] = "MI Cahaya Web Service";
    res.json(data);
});

//  Mengambil Data User
router.get('/user',function (req,res) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query("SELECT * FROM tb_user",function (err,rows,fields) {
        if (rows.length !=0){
            data["error"]=0;
            data["user"]=rows;
            res.json(data);
        }else{
            data["Data User"]='Tidak ada data user...';
            res.json(data);
        }
    })
});

// LOGIN
router.post('/login', function(req, res, next) {

    var username = req.body.username;
    var password = req.body.password;

    connection.query(
        "SELECT * from tb_user where username = ? AND password = ? ",[username, password],function (err,row1,field) {
            if (err){
                console.log(err);
                res.send({'success':false,'message':'Could not connect do db'});
            }
            if(row1.length > 0){
                if(row1[0].level == "guru"){
                    connection.query(
                        "SELECT kelas FROM tb_guru JOIN tb_kelas ON tb_kelas.id_kelas = tb_guru.id_kelas WHERE tb_guru.id_user = ? ",[row1[0].id_user],function (err,row2,field) {
                            if (err){
                                console.log(err);
                                res.send({'success':false,'message':'Could not connect do db'});
                            }
                            if(row2.length > 0){
                                res.send({'success':true,'id':row1[0].id_user,'user':row1[0].username,'level':row1[0].level,'kelas':row2[0].kelas});
                            }else{
                                res.send({'success':false,'message':'user not found'});
                            }
                        }
                    );
                }else if(row1[0].level == "siswa"){
                    connection.query(
                        "SELECT kelas FROM tb_siswa JOIN tb_kelas ON tb_kelas.id_kelas = tb_siswa.id_kelas WHERE tb_siswa.id_user = ? ",[row1[0].id_user],function (err,row2,field) {
                            if (err){
                                console.log(err);
                                res.send({'success':false,'message':'Could not connect do db'});
                            }
                            if(row2.length > 0){
                                res.send({'success':true,'id':row1[0].id_user,'user':row1[0].username,'level':row1[0].level,'kelas':row2[0].kelas});
                            }else{
                                res.send({'success':false,'message':'user not found'});
                            }
                        }
                    );
                }
                // res.send({'success':true,'id':row[0].id_user,'user':row[0].username,'level':row[0].level});
            }else{
                res.send({'success':false,'message':'user not found'});
            }
        }
    );
});

// REGISTRASI TOKEN
router.put('/regisToken',function (req,res){

    var id_user = req.body.id_user;
    var token = req.body.token;
    var data ={
        "error":1,
        "informasi":"",
    };
    if(!!id_user){
        connection.query("UPDATE tb_user SET token = ? WHERE id_user = ?",[token,id_user],function(err,rows,fields){
            if(!!err){
                data["informasi"] = "kesalahan pembaharuan data token";
            }else{
                data["error"] = 0;
                data["informasi"] = "Data token telah berhasil diubah";
            }
            res.json(data);
        });
    }else{
        data["Informasi"] = "inputkan data berdasarkan (i.e : judul,tanggal_publikasi,kontent,sumber";
        res.json(data);
    }
});

// UNREGISTRASI TOKEN
router.put('/unregisToken',function (req,res){

    var id_user = req.body.id_user;
    var data ={
        "error":1,
        "informasi":"",
    };
    if(!!id_user){
        connection.query("UPDATE tb_user SET token = '' WHERE id_user = ?",[id_user],function(err,rows,fields){
            if(!!err){
                data["informasi"] = "kesalahan pembaharuan data token";
            }else{
                data["error"] = 0;
                data["informasi"] = "Data token telah berhasil diubah";
            }
            res.json(data);
        });
    }else{
        data["Informasi"] = "inputkan data berdasarkan (i.e : judul,tanggal_publikasi,kontent,sumber";
        res.json(data);
    }
});

module.exports = router;