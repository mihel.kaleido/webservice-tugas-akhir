var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var moment = require('moment');
var gcm = require('node-gcm');

var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'db_mi_cahaya'
});
var global_json_data;

//  Sebagai Index page
router.get('/',function (req,res) {
    var data = {
        "Data":""
    };
    data["Data"] = "MI Cahaya Web Service";
    res.json(data);
});

// Tampil Semua kegiatan
router.get('/tampilSemuaKegiatan',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data Kegiatan tidak ada/kosong "
    };
    connection.query('SELECT * FROM tb_kegiatan', function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
        }
        else { // if activity found
            res.json(rows)
        }
    })
});

//STATISTIK

// Tampil kegiatan mengaji /kelas bulan ini untuk statistik
router.get('/tampilKegiatanMengaji_S/(:bulan)/(:kelas)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong",
        "mengaji": 0,
        "t_mengaji": 100,
    };
    var dataSiswa = undefined;


    //DAFTAR SISWA KELAS
    connection.query('SELECT tb_siswa.id_siswa,tb_siswa.nama,tb_kelas.kelas FROM `tb_siswa` JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE kelas = '+"'"+ req.params.kelas +"'", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
            // res.redirect('/users')
        }
        else { // if activity found
            dataSiswa = rows;
            // res.json(dataSiswa);

            // KEGIATAN SISWA KELAS
            connection.query('SELECT * FROM `tb_kegiatan` JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%' && kelas = '"+req.params.kelas+"'", function(err, rows, fields) {
                if(err) throw err

                // if activity not found
                if (rows.length <= 0) {
                    // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                    res.json(data)
                }
                else { // if activity found
                    var mengaji = 0;
                    for(var i=0; i<rows.length;i++){
                        if(rows[i]['bacaanAwal'] != "" && rows[i]['bacaanAkhir'] != ""){
                            mengaji = mengaji + 1;
                        }
                    };
                    mengaji = ((mengaji/rows.length)*100)/dataSiswa.length;
                    var t_mengaji = 100-mengaji;
                    var statistik = {
                        "mengaji": mengaji,
                        "t_mengaji": t_mengaji,
                    };

                    res.json(statistik);
                }
            })
        }
    })
});

// Tampil kegiatan salat /kelas bulan ini untuk statistik
router.get('/tampilKegiatanSalat_S/(:bulan)/(:kelas)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong",
        "subuh": 0,
        "dzuhur": 0,
        "ashar": 0,
        "maghrib": 0,
        "isya": 0,
    };
    var dataSiswa = undefined;


    //DAFTAR SISWA KELAS
    connection.query('SELECT tb_siswa.id_siswa,tb_siswa.nama,tb_kelas.kelas FROM `tb_siswa` JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE kelas = '+"'"+ req.params.kelas +"'", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
            // res.redirect('/users')
        }
        else { // if activity found
            dataSiswa = rows;
            // res.json(dataSiswa);

            // KEGIATAN SISWA KELAS
            connection.query('SELECT * FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%' && kelas = '"+req.params.kelas+"'", function(err, rows, fields) {
                if(err) throw err

                // if activity not found
                if (rows.length <= 0) {
                    // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                    res.json(data)
                    // res.redirect('/users')
                }
                else { // if activity found
                    var salat = {
                        "subuh": 0,
                        "dzuhur": 0,
                        "ashar": 0,
                        "maghrib": 0,
                        "isya": 0,
                    };
                    for(var i=0; i<rows.length;i++){
                        if(rows[i]['s_subuh'] == 0 ){
                            salat["subuh"] += 1;
                        }
                        if(rows[i]['s_dzuhur'] == 0 ){
                            salat["dzuhur"] += 1;
                        }
                        if(rows[i]['s_ashar'] == 0 ){
                            salat["ashar"] += 1;
                        }
                        if(rows[i]['s_maghrib'] == 0 ){
                            salat["maghrib"] += 1;
                        }
                        if(rows[i]['s_isya'] == 0 ){
                            salat["isya"] += 1;
                        }
                    };
                    salat['subuh'] = (((salat['subuh'])/rows.length)*100)/dataSiswa.length;
                    salat['dzuhur'] = (((salat['dzuhur'])/rows.length)*100)/dataSiswa.length;
                    salat['ashar'] = (((salat['ashar'])/rows.length)*100)/dataSiswa.length;
                    salat['maghrib'] = (((salat['maghrib'])/rows.length)*100)/dataSiswa.length;
                    salat['isya'] = (((salat['isya'])/rows.length)*100)/dataSiswa.length;

                    var statistik = salat;

                    res.json(statistik)
                }
            })
        }
    })
});

// Tampil kegiatan mengaji /siswa bulan ini untuk statistik
router.get('/tampilKegiatanMengaji_S_Siswa/(:bulan)/(:id_user_siswa)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong",
        "mengaji": 0,
        "t_mengaji": 100,
    };
    connection.query('SELECT tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%' && id_user = "+ req.params.id_user_siswa, function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
        }
        else { // if activity found
            var mengaji = 0;
            for(var i=0; i<rows.length;i++){
                if(rows[i]['bacaanAwal'] != "" && rows[i]['bacaanAkhir'] != ""){
                    mengaji = mengaji + 1;
                }
            };
            mengaji = (mengaji/rows.length)*100;
            var t_mengaji = 100-mengaji;
            var statistik = {
                "mengaji": mengaji,
                "t_mengaji": t_mengaji,
            };

            res.json(statistik);
        }
    })
});

// Tampil kegiatan salat /siswa bulan ini untuk statistik
router.get('/tampilKegiatanSalat_S_Siswa/(:bulan)/(:id_user_siswa)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong",
        "subuh": 0,
        "dzuhur": 0,
        "ashar": 0,
        "maghrib": 0,
        "isya": 0,
    };
    connection.query('SELECT tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%' && id_user = "+ req.params.id_user_siswa, function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
            // res.redirect('/users')
        }
        else { // if activity found
            var salat = {
                "subuh": 0,
                "dzuhur": 0,
                "ashar": 0,
                "maghrib": 0,
                "isya": 0,
            };
            for(var i=0; i<rows.length;i++){
                if(rows[i]['s_subuh'] == 0 ){
                    salat["subuh"] += 1;
                }
                if(rows[i]['s_dzuhur'] == 0 ){
                    salat["dzuhur"] += 1;
                }
                if(rows[i]['s_ashar'] == 0 ){
                    salat["ashar"] += 1;
                }
                if(rows[i]['s_maghrib'] == 0 ){
                    salat["maghrib"] += 1;
                }
                if(rows[i]['s_isya'] == 0 ){
                    salat["isya"] += 1;
                }
            };
            salat['subuh'] = ((salat['subuh'])/rows.length)*100;
            salat['dzuhur'] = ((salat['dzuhur'])/rows.length)*100;
            salat['ashar'] = ((salat['ashar'])/rows.length)*100;
            salat['maghrib'] = ((salat['maghrib'])/rows.length)*100;
            salat['isya'] = ((salat['isya'])/rows.length)*100;

            var statistik = salat;

            res.json(statistik)
        }
    })
});

//END STATISTIK

// Tampil kegiatan mengaji bulan ini
router.get('/tampilKegiatanMengaji_Siswa/(:id_user_siswa)/(:bulan)/(:tanggalAkhir)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query('SELECT tanggal,bacaanAwal,bacaanAkhir FROM tb_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%'"+" && id_user = "+ req.params.id_user_siswa+" ORDER BY `tanggal` ASC", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            // res.json(data)
            var dataKegiatan = [];
            var tanggal = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31",]
            for(var i = 1; i <= req.params.tanggalAkhir ; i++){

                dataKegiatan.push({
                    "tanggal": tanggal[i],
                    "s_subuh": "",
                    "s_dzuhur": "",
                    "s_ashar": "",
                    "s_maghrib": "",
                    "s_isya": ""
                    ,})
            }
            res.json(dataKegiatan);
        }
        else { // if activity found
            // res.json(rows[0]['tanggal'].toString().substring(8,10));
            var dataKegiatan = [];
            var tanggal = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31",]
            var ind = 0;
            for(var i = 1; i <= req.params.tanggalAkhir ; i++){
                // for(var j = 0; j< rows.length; j++){
                if(rows[ind]['tanggal'].toString().substring(8,10) == tanggal[i]){
                    dataKegiatan.push({
                        "tanggal": rows[ind]['tanggal'].toString().substring(8,10),
                        "bacaanAwal": rows[ind]['bacaanAwal'],
                        "bacaanAkhir": rows[ind]['bacaanAkhir'],
                    })
                    ind++;
                }else{
                    dataKegiatan.push({
                        "tanggal": tanggal[i],
                        "bacaanAwal": "",
                        "bacaanAkhir": ""
                        ,})
                }
            }
            res.json(dataKegiatan);
        }
    })
});

// Tampil kegiatan salat bulan ini
router.get('/tampilKegiatanSalat_Siswa/(:id_user_siswa)/(:bulan)/(:tanggalAkhir)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query('SELECT tanggal,s_subuh,s_dzuhur,s_ashar,s_maghrib,s_isya FROM tb_kegiatan JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%'"+" && id_user = "+ req.params.id_user_siswa+" ORDER BY `tanggal` ASC", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            // res.json(data)
            var dataKegiatan = [];
            var tanggal = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31",]
            for(var i = 1; i <= req.params.tanggalAkhir ; i++){

                dataKegiatan.push({
                    "tanggal": tanggal[i],
                    "s_subuh": "",
                    "s_dzuhur": "",
                    "s_ashar": "",
                    "s_maghrib": "",
                    "s_isya": ""
                    ,})
            }
            res.json(dataKegiatan);
        }
        else { // if activity found
            // res.json(rows[0]['tanggal'].toString().substring(8,10));
            var dataKegiatan = [];
            var tanggal = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31",]
            var ind = 0;
            for(var i = 1; i <= req.params.tanggalAkhir ; i++){
                // for(var j = 0; j< rows.length; j++){
                if(rows[ind]['tanggal'].toString().substring(8,10) == tanggal[i]){
                    dataKegiatan.push({
                        "tanggal": rows[ind]['tanggal'].toString().substring(8,10),
                        "s_subuh": rows[ind]['s_subuh'],
                        "s_dzuhur": rows[ind]['s_dzuhur'],
                        "s_ashar": rows[ind]['s_ashar'],
                        "s_maghrib": rows[ind]['s_maghrib'],
                        "s_isya": rows[ind]['s_isya'],
                    })
                    ind++;
                }else{
                    dataKegiatan.push({
                        "tanggal": tanggal[i],
                        "s_subuh": "",
                        "s_dzuhur": "",
                        "s_ashar": "",
                        "s_maghrib": "",
                        "s_isya": ""
                        ,})
                }

                // }
                // if(rows[i-1]['tanggal'].toString().substring(8,10) == tanggal[i]){
                //     dataKegiatan.push({
                //         "tanggal": rows[i-1]['tanggal'].toString().substring(8,10),
                //         "s_subuh": rows[i-1]['s_subuh'],
                //         "s_dzuhur": rows[i-1]['s_dzuhur'],
                //         "s_ashar": rows[i-1]['s_ashar'],
                //         "s_maghrib": rows[i-1]['s_maghrib'],
                //         "s_isya": rows[i-1]['s_isya'],
                //     })
                // }else{
                //     dataKegiatan.push({
                //         "tanggal": tanggal[i],
                //         "s_subuh": "",
                //         "s_dzuhur": "",
                //         "s_ashar": "",
                //         "s_maghrib": "",
                //         "s_isya": ""
                //         ,})
                // }

            }
            res.json(dataKegiatan);
        }
    })
});

// Tampil kegiatan salat, mengaji, dan catatan bulan ini
router.get('/tampilKegiatan_Siswa/(:id_user_siswa)/(:tgl)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query('SELECT tb_kegiatan.id_kegiatan, tanggal,s_subuh,s_dzuhur,s_ashar,s_maghrib,s_isya,bacaanAwal,bacaanAkhir,catatan_ortu FROM tb_kegiatan JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_catatan ON tb_kegiatan.id_kegiatan = tb_catatan.id_kegiatan WHERE id_user = '+ req.params.id_user_siswa+" && tanggal LIKE '"+req.params.tgl+"' ORDER BY `tanggal` ASC LIMIT 1", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)

            res.json(data)
        }
        else { // if activity found
            // res.json(rows[0]['tanggal'].toString().substring(8,10));
            res.json(rows[0]);
        }
    })
});

// Tampil tanggal kegiatan salat, mengaji, dan catatan bulan ini
router.get('/tampilTanggalKegiatan_Siswa/(:id_user_siswa)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query('SELECT tanggal FROM tb_kegiatan JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_catatan ON tb_kegiatan.id_kegiatan = tb_catatan.id_kegiatan WHERE id_user = '+ req.params.id_user_siswa+" ORDER BY `tanggal` ASC", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
        }
        else { // if activity found
            // res.json(rows[0]['tanggal'].toString().substring(8,10));
            data = {
                "error": 0,
                "user": "Data ada"
            };
            var tanggal= [];
            for(var i = 0 ; i < rows.length ; i++){
                tanggal.push(moment(rows[i]['tanggal'].toLocaleDateString()).format('YYYY-MM-DD'))
            }
            res.json(tanggal);
        }
    })
});

// Tampil laporan kegiatan semester ganjil / genap
router.get('/tampilLaporan/(:id_user_siswa)/(:semester)', function (req,res,next){

    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    var dataSiswa = undefined;
    var dataKegiatan = undefined;
    var laporan_bulanan = [];
    var p_subuh = 0;
    var p_dzuhur = 0;
    var p_ashar = 0;
    var p_maghrib = 0;
    var p_isya = 0;
    var p_salat = 0;
    var p_mengaji = 0;
    var r_p_salat = 0;
    var r_p_mengaji= 0;
    var dataKegiatan_temp = [];
    var data_p_siswa = [];
    var bulan = ["01","02","03","04","05","06","07","08","09","10","11","12"]

    if(req.params.semester == "genap"){
        //DATA SISWA
        connection.query('SELECT tb_siswa.id_siswa,tb_siswa.nama FROM `tb_siswa` WHERE id_user = '+ req.params.id_user_siswa, function(err, rows, fields) {
            if(err) throw err

            // if activity not found
            if (rows.length <= 0) {
                // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                res.json(data)
                // res.redirect('/users')
            }
            else { // if activity found
                dataSiswa = rows;
                // res.json(dataSiswa);
                // for(var k = 0; k <= 5 ;k++){
                // KEGIATAN SISWA KELAS
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[0] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                            if(err) throw err

                            // if activity not found
                            if (rows.length <= 0) {
                                // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                                // res.json(data)
                                var persentase = [bulan[0],0,0];
                                laporan_bulanan.push(persentase);
                                // res.json(p)
                                // res.redirect('/users')
                            }
                            else { // if activity found
                                // res.json(rows[0]['id_siswa'])
                                dataKegiatan = rows;
                                for(var i = 0; i<dataSiswa.length;i++){
                                    for (var j = 0; j < dataKegiatan.length;j++){
                                        if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                            dataKegiatan_temp.push(dataKegiatan[j]);
                                            if( dataKegiatan[j]["s_subuh"] == 0) {
                                                p_subuh += 100;
                                            }
                                            if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                                p_dzuhur += 100;
                                            }
                                            if( dataKegiatan[j]["s_ashar"] == 0) {
                                                p_ashar += 100;
                                            }
                                            if( dataKegiatan[j]["s_maghrib"] == 0) {
                                                p_maghrib += 100;
                                            }
                                            if( dataKegiatan[j]["s_isya"] == 0) {
                                                p_isya += 100;
                                            }
                                            if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                                p_mengaji += 100;
                                            }
                                        }
                                    }
                                    var data_temp = [];
                                    data_temp.push(dataSiswa[i]["nama"]);
                                    p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                                    data_temp.push(p_salat)
                                    data_temp.push(p_mengaji/dataKegiatan_temp.length);
                                    data_p_siswa.push(data_temp);
                                    dataKegiatan_temp = [];
                                    p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                                }
                                for (var i = 0; i< data_p_siswa.length;i++){
                                    if (data_p_siswa[i][1] >= 0){
                                        r_p_salat   += data_p_siswa[i][1];
                                        r_p_mengaji += data_p_siswa[i][2];

                                    }
                                    // r_p_salat = r_p_salat + data_p_siswa[i][1];
                                    // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                                }
                                r_p_salat = r_p_salat/dataSiswa.length;
                                r_p_mengaji = r_p_mengaji/dataSiswa.length;
                                var persentase = [];
                                persentase.push(bulan[0])
                                persentase.push(r_p_salat);
                                persentase.push(r_p_mengaji);
                                laporan_bulanan.push(persentase);
                                // res.json(persentase);
                                // res.json(laporan_bulanan);
                            }
                        })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[1] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[1],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[1])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[2] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[2],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[2])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[3] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[3],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[3])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[4] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[4],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[4])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[5] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[5],0,0];
                        laporan_bulanan.push(persentase);
                        res.json(laporan_bulanan);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[5])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                        res.json(laporan_bulanan);
                    }
                })
                // }
            }
        })
    }else if(req.params.semester == "ganjil"){
        //DATA SISWA
        connection.query('SELECT tb_siswa.id_siswa,tb_siswa.nama FROM `tb_siswa` WHERE id_user = '+ req.params.id_user_siswa, function(err, rows, fields) {
            if(err) throw err

            // if activity not found
            if (rows.length <= 0) {
                // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                res.json(data)
                // res.redirect('/users')
            }
            else { // if activity found
                dataSiswa = rows;
                // res.json(dataSiswa);
                // for(var k = 0; k <= 5 ;k++){
                // KEGIATAN SISWA KELAS
                for(var i = 6 ; i<=11;i++){

                }
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[6] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[6],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(p)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[6])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                        // res.json(laporan_bulanan);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[7] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[7],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[7])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[8] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[8],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[8])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[9] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[9],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[9])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[10] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[10],0,0];
                        laporan_bulanan.push(persentase);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[10])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                    }
                })
                connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ bulan[11] +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                    if(err) throw err

                    // if activity not found
                    if (rows.length <= 0) {
                        // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                        // res.json(data)
                        var persentase = [bulan[11],0,0];
                        laporan_bulanan.push(persentase);
                        res.json(laporan_bulanan);
                        // res.json(persentase)
                        // res.redirect('/users')
                    }
                    else { // if activity found
                        // res.json(rows[0]['id_siswa'])
                        dataKegiatan = rows;
                        for(var i = 0; i<dataSiswa.length;i++){
                            for (var j = 0; j < dataKegiatan.length;j++){
                                if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                    dataKegiatan_temp.push(dataKegiatan[j]);
                                    if( dataKegiatan[j]["s_subuh"] == 0) {
                                        p_subuh += 100;
                                    }
                                    if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                        p_dzuhur += 100;
                                    }
                                    if( dataKegiatan[j]["s_ashar"] == 0) {
                                        p_ashar += 100;
                                    }
                                    if( dataKegiatan[j]["s_maghrib"] == 0) {
                                        p_maghrib += 100;
                                    }
                                    if( dataKegiatan[j]["s_isya"] == 0) {
                                        p_isya += 100;
                                    }
                                    if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                        p_mengaji += 100;
                                    }
                                }
                            }
                            var data_temp = [];
                            data_temp.push(dataSiswa[i]["nama"]);
                            p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                            data_temp.push(p_salat)
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                            data_p_siswa.push(data_temp);
                            dataKegiatan_temp = [];
                            p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        }
                        for (var i = 0; i< data_p_siswa.length;i++){
                            if (data_p_siswa[i][1] >= 0){
                                r_p_salat   += data_p_siswa[i][1];
                                r_p_mengaji += data_p_siswa[i][2];

                            }
                            // r_p_salat = r_p_salat + data_p_siswa[i][1];
                            // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                        }
                        r_p_salat = r_p_salat/dataSiswa.length;
                        r_p_mengaji = r_p_mengaji/dataSiswa.length;
                        var persentase = [];
                        persentase.push(bulan[11])
                        persentase.push(r_p_salat);
                        persentase.push(r_p_mengaji);
                        laporan_bulanan.push(persentase);
                        // res.json(persentase);
                        res.json(laporan_bulanan);
                    }
                })
                // }
            }
        })
    }
});

// Detail Siswa
router.get('/tampilDetailSiswa/(:idSiswa)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query('SELECT * FROM siswa WHERE id_siswa = '+req.params.idSiswa, function(err, rows, fields) {
        if(err) throw err
        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
        }
        else { // if activity found
            res.json(rows)
        }
    })
});

// Tambah Kegiatan Baru (salat, mengaji, catatan / untuk siswa) POST ACTION
router.post('/tambahkegiatan',function (req,res){ // POST digunakan utk tambah data

    var id_kegiatan = 0;
    var id_user = req.body.id_user;
    var tanggal = req.body.tanggal;
    if(req.body.s_subuh == 1){
        var s_subuh = 0;
    }else{
        var s_subuh = undefined;
    }
    if(req.body.s_dzuhur == 1){
        var s_dzuhur = 0;
    }else{
        var s_dzuhur = undefined;
    }
    if(req.body.s_ashar == 1){
        var s_ashar = 0;
    }else{
        var s_ashar = undefined;
    }
    if(req.body.s_maghrib == 1){
        var s_maghrib = 0;
    }else{
        var s_maghrib = undefined;
    }
    if(req.body.s_isya == 1){
        var s_isya = 0;
    }else{
        var s_isya = undefined;
    }
    var m_awal = req.body.m_awal;
    var m_akhir = req.body.m_akhir;
    var catatan = req.body.catatan;

    var data ={
        "error":1,
        "id_kegiatan":"",
        "InformasiKegiatan":"",
        "InformasiSalat":"",
        "InformasiMengaji":"",
        "InformasiCatatan":"",
    };

    connection.query("SELECT id_siswa FROM tb_siswa WHERE id_user = "+id_user,function (err,rows,fields) {
        if(!!err){
            data["id_siswa"] = "kesalahan pencarian data id_siswa";
        }else{
            // res.json(rows[0]['id_siswa']);
            var id_siswa = rows[0]['id_siswa'];
            if(!!id_siswa && !!tanggal){
                connection.query
                (
                    "INSERT INTO tb_kegiatan (id_siswa, tanggal) VALUES (?,?)",[id_siswa,tanggal],
                    function(err,rows,fields){
                        if(!!err){
                            data["InformasiKegiatan"] = "kesalahan penambahan data";
                        }else{
                            data["error"] = 0;
                            data["InformasiKegiatan"] = "Data kegiatan telah berhasil ditambahkan";
                            data["id_kegiatan"] = rows.insertId;
                            id_kegiatan = rows.insertId;

                            connection.query
                            (
                                "INSERT INTO tb_salat (id_kegiatan,s_subuh,s_dzuhur,s_ashar,s_maghrib,s_isya) VALUES(?,?,?,?,?,?)",[id_kegiatan,s_subuh,s_dzuhur,s_ashar,s_maghrib,s_isya],
                                function (err,rows,fields) {
                                    if(!!err){
                                        data["error"] = 1;
                                        data["InformasiSalat"] = "kesalahan penambahan data salat";
                                    }else {
                                        data["error"] = 0;
                                        data["InformasiSalat"] = "Data kegiatan Salat telah berhasil ditambahkan";
                                    }
                                }
                            );

                            connection.query
                            (
                                "INSERT INTO tb_mengaji (id_kegiatan,bacaanAwal,bacaanAkhir) VALUES(?,?,?)",[id_kegiatan,m_awal,m_akhir],
                                function (err,rows,fields) {
                                    if(!!err){
                                        data["error"] = 1;
                                        data["InformasiMengaji"] = "kesalahan penambahan data mengaji";
                                    }else {
                                        data["error"] = 0;
                                        data["InformasiMengaji"] = "Data kegiatan Mengaji telah berhasil ditambahkan";
                                    }
                                    // res.json(data);
                                    // res.send({'success':true});
                                }
                            );

                            connection.query
                            (
                                "INSERT INTO tb_catatan (id_kegiatan,catatan_ortu) VALUES(?,?)",[id_kegiatan,catatan],
                                function (err,rows,fields) {
                                    if(!!err){
                                        data["error"] = 1;
                                        data["InformasiCatatan"] = "kesalahan penambahan data catatan";
                                    }else {
                                        data["error"] = 0;
                                        data["InformasiCatatan"] = "Data catatan telah berhasil ditambahkan";
                                    }
                                    // res.json(data);
                                    res.send({'success':true,'id_kegiatan':id_kegiatan});
                                }
                            );

                        }
                        // res.json(data);
                    }
                );
            }
            else{
                data["Informasi"] = "inputkan data berdasarkan (i.e : )";
                res.json(data);
            }
        }
    })
});

// Perbaharui Kegiatan (salat, mengaji, catatan / untuk siswa) PUT ACTION
router.put('/perbaharuiKegiatan',function (req,res){

    var id_kegiatan = req.body.id_kegiatan;
    if(req.body.s_subuh == 1){
        var s_subuh = 0;
    }else{
        var s_subuh = undefined;
    }
    if(req.body.s_dzuhur == 1){
        var s_dzuhur = 0;
    }else{
        var s_dzuhur = undefined;
    }
    if(req.body.s_ashar == 1){
        var s_ashar = 0;
    }else{
        var s_ashar = undefined;
    }
    if(req.body.s_maghrib == 1){
        var s_maghrib = 0;
    }else{
        var s_maghrib = undefined;
    }
    if(req.body.s_isya == 1){
        var s_isya = 0;
    }else{
        var s_isya = undefined;
    }
    var m_awal = req.body.m_awal;
    var m_akhir = req.body.m_akhir;
    var catatan_ortu = req.body.catatan
    var data ={
        "error":1,
        "InformasiSalat":"",
        "InformasiMengaji":"",
        "InformasiCatatan":"",
    };

    if(!!id_kegiatan){
        connection.query("UPDATE tb_salat SET s_subuh = ?, s_dzuhur = ?, s_ashar = ?, s_maghrib = ?, s_isya = ? WHERE id_kegiatan = ? ",[s_subuh,s_dzuhur,s_ashar,s_maghrib,s_isya,id_kegiatan],function(err,rows,fields){
            if(!!err){
                data["InformasiSalat"] = "kesalahan pembaharuan data salat";
            }else{
                data["error"] = 0;
                data["InformasiSalat"] = "Data kegiatan salat telah berhasil diubah";
            }
            // res.json(data);
        });

        connection.query("UPDATE tb_mengaji SET bacaanAwal = ?, bacaanAkhir = ? WHERE id_kegiatan = ?",[m_awal,m_akhir,id_kegiatan],function(err,rows,fields){
            if(!!err){
                data["InformasiMengaji"] = "kesalahan pembaharuan data mengaji";
            }else{
                data["error"] = 0;
                data["InformasiMengaji"] = "Data kegiatan mengaji telah berhasil diubah";
            }
            // res.json(data);
        });

        connection.query("UPDATE tb_catatan SET catatan_ortu = ? WHERE id_kegiatan = ? ",[catatan_ortu,id_kegiatan],function(err,rows,fields){
            if(!!err){
                data["InformasiCatatan"] = "kesalahan pembaharuan data catatan";
            }else{
                data["error"] = 0;
                data["InformasiCatatan"] = "Data kegiatan catatan telah berhasil diubah";
            }
            // res.json(data);
            res.send({'success':true});
        });

    }else{
        data["Informasi"] = "inputkan data berdasarkan (i.e : judul,tanggal_publikasi,kontent,sumber";
        res.json(data);
    }
});

// Tampil kegiatan untuk di review(setelah di tambah oleh ortu)
router.get('/tampilKegiatan_review/(:id_kegiatan)', function(req,res,next){
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query('SELECT \n' +
        'tb_siswa.nama, \n' +
        'tb_kelas.kelas, \n' +
        'tb_kegiatan.tanggal, \n' +
        'tb_salat.s_subuh, tb_salat.s_dzuhur, tb_salat.s_ashar, tb_salat.s_maghrib, tb_salat.s_isya, \n' +
        'tb_mengaji.bacaanAwal, tb_mengaji.bacaanAkhir, \n' +
        'tb_catatan.catatan_ortu \n' +
        'FROM tb_kegiatan \n' +
        'JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan \n' +
        'JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan \n' +
        'JOIN tb_catatan ON tb_kegiatan.id_kegiatan = tb_catatan.id_kegiatan\n' +
        'JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa\n' +
        'JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas\n' +
        'WHERE tb_kegiatan.id_kegiatan = '+req.params.id_kegiatan, function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
        }
        else { // if activity found
            var dataSiswa = [{
                nama:	rows[0]['nama'],
                kelas:	rows[0]['kelas'],
                tanggal:	moment(rows[0]['tanggal'].toLocaleDateString()).format('DD-MM-YYYY'),
                s_subuh:	rows[0]['s_subuh'],
                s_dzuhur:	rows[0]['s_dzuhur'],
                s_ashar:	rows[0]['s_ashar'],
                s_maghrib:	rows[0]['s_maghrib'],
                s_isya:	    rows[0]['s_isya'],
                bacaanAwal:	rows[0]['bacaanAwal'],
                bacaanAkhir:rows[0]['bacaanAkhir'],
                catatan_ortu:	rows[0]['catatan_ortu'],}
            ]
            res.json(dataSiswa[0])
        }
    })
});

// Tambah Kegiatan Baru (hafalan / untuk guru) POST ACTION

// Tambah Catatan Kegiatan (untuk guru)


// Peringkat siswa bulan ini
router.get('/tampilPeringkat/(:kelas)/(:bulan)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    var dataSiswa = undefined;
    var dataKegiatan = undefined;
    var p_subuh = 0;
    var p_dzuhur = 0;
    var p_ashar = 0;
    var p_maghrib = 0;
    var p_isya = 0;
    var p_salat = 0;
    var p_mengaji = 0;
    var p_result = 0;
    var dataKegiatan_temp = [];
    var data_p_siswa = [];


    //DAFTAR SISWA KELAS
    connection.query('SELECT tb_siswa.id_siswa,tb_siswa.nama,tb_kelas.kelas FROM `tb_siswa` JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE kelas = '+"'"+ req.params.kelas +"'", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
            // res.redirect('/users')
        }
        else { // if activity found
            dataSiswa = rows;
            // res.json(dataSiswa);

            // KEGIATAN SISWA KELAS
            connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%' && kelas = "+"'"+req.params.kelas+"'"+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                if(err) throw err

                // if activity not found
                if (rows.length <= 0) {
                    // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                    res.json(data)
                    // res.redirect('/users')
                }
                else { // if activity found
                    // res.json(rows[0]['id_siswa'])
                    dataKegiatan = rows;
                    for(var i = 0; i<dataSiswa.length;i++){
                        for (var j = 0; j < dataKegiatan.length;j++){
                            if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                dataKegiatan_temp.push(dataKegiatan[j]);
                                if( dataKegiatan[j]["s_subuh"] == 0) {
                                    p_subuh += 100;
                                }
                                if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                    p_dzuhur += 100;
                                }
                                if( dataKegiatan[j]["s_ashar"] == 0) {
                                    p_ashar += 100;
                                }
                                if( dataKegiatan[j]["s_maghrib"] == 0) {
                                    p_maghrib += 100;
                                }
                                if( dataKegiatan[j]["s_isya"] == 0) {
                                    p_isya += 100;
                                }
                                if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                    p_mengaji += 100;
                                }
                            }
                        }
                        var data_temp = [];
                        data_temp.push(dataSiswa[i]["nama"]);
                        p_result = ((((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5) + (p_mengaji/dataKegiatan_temp.length))/2;
                        data_temp.push(p_result);
                        data_p_siswa.push(data_temp);
                        dataKegiatan_temp = [];
                        data_temp = [];
                        p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                        p_result = 0;
                    }
                    for(var i = 0; i < data_p_siswa.length; i++){
                        for (var j = 0; j < data_p_siswa.length-1; j++) {
                            if(data_p_siswa[j][1]<data_p_siswa[j+1][1]){
                                var temp = data_p_siswa[j];
                                data_p_siswa [j] = data_p_siswa[j+1];
                                data_p_siswa [j+1] = temp;
                            }
                        }
                    }
                    for(var i = 0; i < data_p_siswa.length; i++){
                        if(data_p_siswa[i][1] > 0){
                            // data_p_siswa[i][1].toFixed();
                        }else{
                            data_p_siswa[i][1] = 0;
                        }
                    }
                    res.json(data_p_siswa);
                }
            })
        }
    })
});

// persentase rata2 siswa /kelas bulan ini
router.get('/tampilNilaiRata1_salat/(:kelas)/(:bulan)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    var dataSiswa = undefined;
    var dataKegiatan = undefined;
    var p_subuh = 0;
    var p_dzuhur = 0;
    var p_ashar = 0;
    var p_maghrib = 0;
    var p_isya = 0;
    var p_salat = 0;
    var p_mengaji = 0;
    var r_p_salat = 0;
    var r_p_mengaji= 0;
    var dataKegiatan_temp = [];
    var data_p_siswa = [];


    //DAFTAR SISWA KELAS
    connection.query('SELECT tb_siswa.id_user, tb_siswa.id_siswa,tb_siswa.nama,tb_kelas.kelas FROM `tb_siswa` JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE kelas = '+"'"+ req.params.kelas +"'", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
            // res.redirect('/users')
        }
        else { // if activity found
            dataSiswa = rows;
            // res.json(dataSiswa);

            // KEGIATAN SISWA KELAS
            connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%' && kelas = "+"'"+req.params.kelas+"'"+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                if(err) throw err

                // if activity not found
                if (rows.length <= 0) {
                    // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                    res.json(data)
                    // res.redirect('/users')
                }
                else { // if activity found
                    // res.json(rows[0]['id_siswa'])
                    dataKegiatan = rows;
                    for(var i = 0; i<dataSiswa.length;i++){
                        for (var j = 0; j < dataKegiatan.length;j++){
                            if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                dataKegiatan_temp.push(dataKegiatan[j]);
                                if( dataKegiatan[j]["s_subuh"] == 0) {
                                    p_subuh += 100;
                                }
                                if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                    p_dzuhur += 100;
                                }
                                if( dataKegiatan[j]["s_ashar"] == 0) {
                                    p_ashar += 100;
                                }
                                if( dataKegiatan[j]["s_maghrib"] == 0) {
                                    p_maghrib += 100;
                                }
                                if( dataKegiatan[j]["s_isya"] == 0) {
                                    p_isya += 100;
                                }
                                if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                    p_mengaji += 100;
                                }
                            }
                        }
                        var data_temp = [];
                        data_temp.push(dataSiswa[i]["id_user"]);
                        data_temp.push(dataSiswa[i]["nama"]);
                        // data_temp.push(p_subuh/dataKegiatan.length);
                        // data_temp.push(p_dzuhur/dataKegiatan.length);
                        // data_temp.push(p_ashar/dataKegiatan.length);
                        // data_temp.push(p_maghrib/dataKegiatan.length);
                        // data_temp.push(p_isya/dataKegiatan.length);
                        p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                        if(p_salat >= 0){
                            data_temp.push(p_salat);
                        }else{
                            data_temp.push(0);
                        }
                        if((p_mengaji/dataKegiatan_temp.length) >= 0){
                            data_temp.push(p_mengaji/dataKegiatan_temp.length);
                        }else{
                            data_temp.push(0);
                        }

                        // data_temp.push(r_p_mengaji);
                        data_p_siswa.push(data_temp);
                        dataKegiatan_temp = [];
                        p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                    }
                    res.json(data_p_siswa);
                }
            })

        }
    })
});

// Persentase rata2 siswa /kelas bulan ini dijadiin satu(hasil dari seluruh kelas)
router.get('/tampilNilaiRata2/(:kelas)/(:bulan)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    var dataSiswa = undefined;
    var dataKegiatan = undefined;
    var p_subuh = 0;
    var p_dzuhur = 0;
    var p_ashar = 0;
    var p_maghrib = 0;
    var p_isya = 0;
    var p_salat = 0;
    var p_mengaji = 0;
    var r_p_salat = 0;
    var r_p_mengaji= 0;
    var dataKegiatan_temp = [];
    var data_p_siswa = [];


    //DAFTAR SISWA KELAS
    connection.query('SELECT tb_siswa.id_siswa,tb_siswa.nama,tb_kelas.kelas FROM `tb_siswa` JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE kelas = '+"'"+ req.params.kelas +"'", function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
            // res.redirect('/users')
        }
        else { // if activity found
            dataSiswa = rows;
            // res.json(dataSiswa);

            // KEGIATAN SISWA KELAS
            connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%' && kelas = "+"'"+req.params.kelas+"'"+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                if(err) throw err

                // if activity not found
                if (rows.length <= 0) {
                    // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                    res.json(data)
                    // res.redirect('/users')
                }
                else { // if activity found
                    // res.json(rows[0]['id_siswa'])
                    dataKegiatan = rows;
                    for(var i = 0; i<dataSiswa.length;i++){
                        for (var j = 0; j < dataKegiatan.length;j++){
                            if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                dataKegiatan_temp.push(dataKegiatan[j]);
                                if( dataKegiatan[j]["s_subuh"] == 0) {
                                    p_subuh += 100;
                                }
                                if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                    p_dzuhur += 100;
                                }
                                if( dataKegiatan[j]["s_ashar"] == 0) {
                                    p_ashar += 100;
                                }
                                if( dataKegiatan[j]["s_maghrib"] == 0) {
                                    p_maghrib += 100;
                                }
                                if( dataKegiatan[j]["s_isya"] == 0) {
                                    p_isya += 100;
                                }
                                if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                    p_mengaji += 100;
                                }
                            }
                        }
                        var data_temp = [];
                        data_temp.push(dataSiswa[i]["nama"]);
                        // data_temp.push(p_subuh/dataKegiatan.length);
                        // data_temp.push(p_dzuhur/dataKegiatan.length);
                        // data_temp.push(p_ashar/dataKegiatan.length);
                        // data_temp.push(p_maghrib/dataKegiatan.length);
                        // data_temp.push(p_isya/dataKegiatan.length);
                        p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                        data_temp.push(p_salat)
                        data_temp.push(p_mengaji/dataKegiatan_temp.length);
                        // data_temp.push(r_p_mengaji);
                        data_p_siswa.push(data_temp);
                        dataKegiatan_temp = [];
                        p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                    }
                    for (var i = 0; i< data_p_siswa.length;i++){
                        if (data_p_siswa[i][1] >= 0){
                            r_p_salat   += data_p_siswa[i][1];
                            r_p_mengaji += data_p_siswa[i][2];

                        }
                        // r_p_salat = r_p_salat + data_p_siswa[i][1];
                        // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                    }
                    r_p_salat = r_p_salat/dataSiswa.length;
                    r_p_mengaji = r_p_mengaji/dataSiswa.length;
                    var persentase = [];
                    persentase.push(r_p_salat);
                    persentase.push(r_p_mengaji);
                    res.json(persentase);
                    // res.json(data_p_siswa);
                }
            })

        }
    })
});

// Persentase rata2 siswa  bulan ini
router.get('/tampilNilaiRata2_Siswa/(:id_user_siswa)/(:bulan)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    var dataSiswa = undefined;
    var dataKegiatan = undefined;
    var p_subuh = 0;
    var p_dzuhur = 0;
    var p_ashar = 0;
    var p_maghrib = 0;
    var p_isya = 0;
    var p_salat = 0;
    var p_mengaji = 0;
    var r_p_salat = 0;
    var r_p_mengaji= 0;
    var dataKegiatan_temp = [];
    var data_p_siswa = [];


    //DAFTAR SISWA KELAS
    connection.query('SELECT tb_siswa.id_siswa,tb_siswa.nama FROM `tb_siswa` WHERE id_user = '+ req.params.id_user_siswa, function(err, rows, fields) {
        if(err) throw err

        // if activity not found
        if (rows.length <= 0) {
            // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
            res.json(data)
            // res.redirect('/users')
        }
        else { // if activity found
            dataSiswa = rows;
            // res.json(dataSiswa);

            // KEGIATAN SISWA KELAS
            connection.query('SELECT tb_kegiatan.id_kegiatan,tb_kegiatan.id_siswa, tb_kelas.kelas, tb_kegiatan.tanggal,tb_salat.s_subuh,tb_salat.s_dzuhur,tb_salat.s_ashar,tb_salat.s_maghrib,tb_salat.s_isya, tb_mengaji.bacaanAwal,tb_mengaji.bacaanAkhir FROM `tb_kegiatan` JOIN tb_salat ON tb_kegiatan.id_kegiatan = tb_salat.id_kegiatan JOIN tb_mengaji ON tb_kegiatan.id_kegiatan = tb_mengaji.id_kegiatan JOIN tb_siswa ON tb_siswa.id_siswa = tb_kegiatan.id_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tanggal LIKE '+"'%-"+ req.params.bulan +"-%' && id_user = "+req.params.id_user_siswa+" ORDER BY `tb_kegiatan`.`id_siswa` ASC", function(err, rows, fields) {
                if(err) throw err

                // if activity not found
                if (rows.length <= 0) {
                    // req.flash('error', 'Kegiatan tidak ditemukan untuk Bulan = ' + req.params.bulan)
                    // res.json(data)
                    var persentase = [0,0];
                    res.json(persentase)
                    // res.redirect('/users')
                }
                else { // if activity found
                    // res.json(rows[0]['id_siswa'])
                    dataKegiatan = rows;
                    for(var i = 0; i<dataSiswa.length;i++){
                        for (var j = 0; j < dataKegiatan.length;j++){
                            if(dataSiswa[i]['id_siswa'] == dataKegiatan[j]['id_siswa']){
                                dataKegiatan_temp.push(dataKegiatan[j]);
                                if( dataKegiatan[j]["s_subuh"] == 0) {
                                    p_subuh += 100;
                                }
                                if( dataKegiatan[j]["s_dzuhur"] == 0) {
                                    p_dzuhur += 100;
                                }
                                if( dataKegiatan[j]["s_ashar"] == 0) {
                                    p_ashar += 100;
                                }
                                if( dataKegiatan[j]["s_maghrib"] == 0) {
                                    p_maghrib += 100;
                                }
                                if( dataKegiatan[j]["s_isya"] == 0) {
                                    p_isya += 100;
                                }
                                if(dataKegiatan[j]["bacaanAwal"] && dataKegiatan[j]["bacaanAkhir"] != ""){
                                    p_mengaji += 100;
                                }
                            }
                        }
                        var data_temp = [];
                        data_temp.push(dataSiswa[i]["nama"]);
                        // data_temp.push(p_subuh/dataKegiatan.length);
                        // data_temp.push(p_dzuhur/dataKegiatan.length);
                        // data_temp.push(p_ashar/dataKegiatan.length);
                        // data_temp.push(p_maghrib/dataKegiatan.length);
                        // data_temp.push(p_isya/dataKegiatan.length);
                        p_salat = ((p_subuh/dataKegiatan_temp.length)+(p_dzuhur/dataKegiatan_temp.length)+(p_ashar/dataKegiatan_temp.length)+(p_maghrib/dataKegiatan_temp.length)+(p_isya/dataKegiatan_temp.length))/5
                        data_temp.push(p_salat)
                        data_temp.push(p_mengaji/dataKegiatan_temp.length);
                        // data_temp.push(r_p_mengaji);
                        data_p_siswa.push(data_temp);
                        dataKegiatan_temp = [];
                        p_subuh = 0;p_dzuhur= 0;p_ashar= 0;p_maghrib= 0;p_isya= 0;p_salat = 0; p_mengaji = 0;
                    }
                    for (var i = 0; i< data_p_siswa.length;i++){
                        if (data_p_siswa[i][1] >= 0){
                            r_p_salat   += data_p_siswa[i][1];
                            r_p_mengaji += data_p_siswa[i][2];

                        }
                        // r_p_salat = r_p_salat + data_p_siswa[i][1];
                        // r_p_mengaji = r_p_mengaji + data_p_siswa[i][2];
                    }
                    r_p_salat = r_p_salat/dataSiswa.length;
                    r_p_mengaji = r_p_mengaji/dataSiswa.length;
                    var persentase = [];
                    persentase.push(r_p_salat);
                    persentase.push(r_p_mengaji);
                    res.json(persentase);
                    // res.json(data_p_siswa);
                }
            })

        }
    })
});



// Catatan Orang Tua Dashboard
router.get('/tampilCatatan/(:kelas)/',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };

    //DAFTAR Catatan Orang Tua
    connection.query(
        'SELECT id_catatan,nama,tanggal,catatan_ortu AS pesan FROM `tb_catatan`\n ' +
        'JOIN tb_kegiatan ON tb_catatan.id_kegiatan = tb_kegiatan.id_kegiatan\n' +
        'JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa\n' +
        'JOIN tb_kelas ON tb_kelas.id_kelas = tb_siswa.id_kelas\n' +
        'WHERE kelas = ' + "'" + req.params.kelas +
        "' ORDER BY `tb_catatan`.`id_catatan`  DESC LIMIT 10" , function(err, rows, fields) {
            if(err) throw err

            // if activity not found
            if (rows.length <= 0) {
                // res.json(data)
                var catatan = [
                    {
                        "id_catatan": "-",
                        "nama": "-",
                        "tanggal": "-",
                        "pesan": "-"
                    },
                ];
                res.json(catatan)
            }else { // if activity found
                var catatan = [];
                for(var i = 0 ; i<rows.length ; i++){
                    catatan.push(
                        {
                            "id_catatan": rows[i]['id_catatan'],
                            "nama": rows[i]['nama'],
                            "tanggal": moment(rows[i]['tanggal'].toLocaleDateString()).format('DD-MM-YYYY'),
                            "pesan": rows[i]['pesan']
                        }
                    )
                }
                res.json(catatan);
            }
        })
});

// Catatan Guru Dashboard
router.get('/tampilCatatan_Guru/(:id_user_siswa)/',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };

    //DAFTAR Catatan Guru
    connection.query(
        'SELECT tb_guru.nama,tanggal,catatan_guru AS pesan FROM `tb_catatan`\n ' +
        'JOIN tb_kegiatan ON tb_catatan.id_kegiatan = tb_kegiatan.id_kegiatan\n' +
        'JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa\n' +
        'JOIN tb_kelas ON tb_kelas.id_kelas = tb_siswa.id_kelas\n' +
        'JOIN tb_guru ON tb_kelas.id_kelas = tb_guru.id_kelas\n' +
        'WHERE tb_siswa.id_user = ' + "'" + req.params.id_user_siswa +
        "' ORDER BY `tb_catatan`.`id_catatan`  DESC LIMIT 10" , function(err, rows, fields) {
            if(err) throw err

            // if activity not found
            if (rows.length <= 0) {
                // res.json(data)
                var catatan = [
                    {
                        "nama": "-",
                        "tanggal": "-",
                        "pesan": "-"
                    },
                ];
                res.json(catatan)
            }else { // if activity found
                var catatan = [];
                for(var i = 0 ; i<rows.length ; i++){
                    catatan.push(
                        {
                            "nama": rows[i]['nama'],
                            "tanggal": moment(rows[i]['tanggal'].toLocaleDateString()).format('DD-MM-YYYY'),
                            "pesan": rows[i]['pesan']
                        }
                    )
                }
                res.json(catatan);
            }
        })
});

// Catatan Orang Tua (halaman catatan)
router.get('/tampilCatatan_List/(:kelas)/',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };

    //DAFTAR Catatan Orang Tua
    connection.query(
        'SELECT id_catatan,nama,tanggal,catatan_ortu AS pesan FROM `tb_catatan`\n ' +
        'JOIN tb_kegiatan ON tb_catatan.id_kegiatan = tb_kegiatan.id_kegiatan\n' +
        'JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa\n' +
        'JOIN tb_kelas ON tb_kelas.id_kelas = tb_siswa.id_kelas\n' +
        'WHERE kelas = ' + "'" + req.params.kelas +
        "' ORDER BY `tb_catatan`.`id_catatan` DESC" , function(err, rows, fields) {
            if(err) throw err

            // if activity not found
            if (rows.length <= 0) {
                // res.json(data)
                var catatan = [
                    {
                        "id_catatan": "-",
                        "nama": "-",
                        "tanggal": "-",
                        "pesan": "-"
                    },
                ];
                res.json(catatan)
            }else { // if activity found
                var catatan = [];
                for(var i = 0 ; i<rows.length ; i++){
                    catatan.push(
                        {
                            "id_catatan": rows[i]['id_catatan'],
                            "nama": rows[i]['nama'],
                            "tanggal": moment(rows[i]['tanggal'].toLocaleDateString()).format('DD-MM-YYYY'),
                            "pesan": rows[i]['pesan']
                        }
                    )
                }
                res.json(catatan);
            }
        })
});

// Catatan Guru (halaman Catatan)
router.get('/tampilCatatan_Guru_List/(:id_user_siswa)/',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };

    //DAFTAR Catatan Guru
    connection.query(
        'SELECT id_catatan,tb_guru.nama,tanggal,catatan_guru AS pesan FROM `tb_catatan`\n ' +
        'JOIN tb_kegiatan ON tb_catatan.id_kegiatan = tb_kegiatan.id_kegiatan\n' +
        'JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa\n' +
        'JOIN tb_kelas ON tb_kelas.id_kelas = tb_siswa.id_kelas\n' +
        'JOIN tb_guru ON tb_kelas.id_kelas = tb_guru.id_kelas\n' +
        'WHERE tb_siswa.id_user = ' + "'" + req.params.id_user_siswa +
        "' ORDER BY `tb_catatan`.`id_catatan` DESC" , function(err, rows, fields) {
            if(err) throw err

            // if activity not found
            if (rows.length <= 0) {
                // res.json(data)
                var catatan = [
                    {
                        "id_catatan": "-",
                        "nama": "-",
                        "tanggal": "-",
                        "pesan": "-"
                    },
                ];
                res.json(catatan)
            }else { // if activity found
                var catatan = [];
                for(var i = 0 ; i<rows.length ; i++){
                    catatan.push(
                        {
                            "id_catatan": rows[i]['id_catatan'],
                            "nama": rows[i]['nama'],
                            "tanggal": moment(rows[i]['tanggal'].toLocaleDateString()).format('DD-MM-YYYY'),
                            "pesan": rows[i]['pesan']
                        }
                    )
                }
                res.json(catatan);
            }
        })
});

// Catatan Harian untuk review (halaman catatan harian)
router.get('/tampilCatatan_review/(:id_catatan)/',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };

    //DAFTAR Catatan Orang Tua
    connection.query(
        'SELECT id_catatan,tanggal,nama, catatan_ortu, catatan_guru  FROM `tb_catatan`\n ' +
        'JOIN tb_kegiatan ON tb_kegiatan.id_kegiatan = tb_catatan.id_kegiatan\n' +
        'JOIN tb_siswa ON tb_kegiatan.id_siswa = tb_siswa.id_siswa\n' +
        'WHERE id_catatan = '+ req.params.id_catatan , function(err, rows, fields) {
            if(err) throw err

            // if activity not found
            if (rows.length <= 0) {
                // res.json(data)
                var catatan = [
                    {
                        "id_catatan": "-",
                        "tanggal": "-",
                        "nama_ortu": "-",
                        "catatan_ortu": "-",
                        "catatan_guru": "-",
                    },
                ];
                res.json(catatan)
            }else { // if activity found
                var catatan = [];
                for(var i = 0 ; i<rows.length ; i++){
                    catatan.push(
                        {
                            "id_catatan": rows[i]['id_catatan'],
                            "tanggal": moment(rows[i]['tanggal'].toLocaleDateString()).format('DD-MM-YYYY'),
                            "nama_ortu": rows[i]['nama'],
                            "catatan_ortu": rows[i]['catatan_ortu'],
                            "catatan_guru": rows[i]['catatan_guru'],
                        }
                    )
                }
                res.json(catatan[0]);
            }
        })
});

// Tambah Catatan Guru
router.put('/tambahCatatanGuru',function (req,res){

    var id_catatan = req.body.id_catatan;
    var catatan_guru = req.body.catatan_guru;
    var data ={
        "error":1,
        "InformasiCatatan":"",
    };

    if(!!id_catatan){
        connection.query("UPDATE tb_catatan SET catatan_guru = ? WHERE id_catatan = ? ",[catatan_guru,id_catatan],function(err,rows,fields){
            if(!!err){
                data["InformasiCatatan"] = "kesalahan pembaharuan data catatan";
            }else{
                data["error"] = 0;
                data["InformasiCatatan"] = "Data kegiatan catatan telah berhasil diubah";
            }
            // res.json(data);
            res.send({'success':true});
        });

    }else{
        data["Informasi"] = "inputkan data berdasarkan (i.e : judul,tanggal_publikasi,kontent,sumber";
        res.json(data);
    }
});



//  Send Notif
router.post('/sendNotifHafalan/(:kelas)',function (req,res) {
    var data = {
        "success": 1,
    };

    var id_user = req.body.id_user;
    var h_awal = req.body.h_awal;
    var h_akhir = req.body.h_akhir;

    var message = new gcm.Message();

    message.addData('hello', 'world');
    message.addNotification('title', 'Hafalan');
    message.addNotification('icon', 'ic_launcher');
    message.addNotification('body', h_awal +" - "+h_akhir);

    //Add your mobile device registration tokens here

    connection.query("SELECT token, tb_siswa.nama FROM tb_guru\n" +
        "JOIN tb_kelas ON tb_guru.id_kelas = tb_kelas.id_kelas\n" +
        "JOIN tb_siswa ON tb_siswa.id_kelas = tb_kelas.id_kelas\n" +
        "JOIN tb_user ON tb_siswa.id_user = tb_user.id_user\n" +
        "WHERE tb_guru.id_user ="+id_user ,function (err,rows,fields) {
        var regTokens = [];
        if (rows.length !=0){
            // data["error"]=0;
            // data["user"]=rows;
            for(var i = 0; i<rows.length;i++){
                regTokens.push(rows[i]['token'])
            }

            //Replace your developer API key with GCM enabled here
            var sender = new gcm.Sender('AAAApIIX2aM:APA91bF1Po98Qxz7z9jKIS-F6e0WbAS-g9zDFDHhrV4E01fip0xeYdLko2pLShtM1UEd5fC9hPutfO1S2dH9Nur66H-plIN4vP3Cd3NrvCV-mM9hAN3Lbu-hqTL94PT45L-Ib8RZFTLx');


            sender.send(message, regTokens, function (err, response) {
                if(err) {
                    console.error(err);
                } else {
                    console.log(response);
                }
            });

            // res.json(regTokens);
            res.send({'success':true});

        }else{
            data["Data Siswa"]='Tidak ada data siswa...';
            res.json(data);
        }



    })
});

module.exports = router;