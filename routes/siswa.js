var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'db_mi_cahaya'
});
var global_json_data;
//  Sebagai Index page
router.get('/',function (req,res) {
    var data = {
        "Data":""
    };
    data["Data"] = "MI Cahaya Web Service";
    res.json(data);
});

//  Mengambil Data siswa
router.get('/(:id_user)',function (req,res) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query("SELECT tb_siswa.nama,tb_kelas.kelas FROM tb_user JOIN tb_siswa JOIN tb_kelas ON tb_user.id_user = tb_siswa.id_user && tb_kelas.id_kelas = tb_siswa.id_kelas WHERE tb_user.id_user = "+req.params.id_user,function (err,rows,fields) {
        if (rows.length !=0){
            res.json(rows);
        }else{
            data["Data Siswa"]='Tidak ada data siswa...';
            res.json(data);
        }
    })
});

//  Mengambil Data list siswa
router.get('/daftarSiswa/(:kelas)',function (req,res) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
    connection.query("SELECT tb_siswa.* FROM tb_siswa INNER JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE tb_kelas.kelas = '"+req.params.kelas+"'",function (err,rows,fields) {
        if (rows.length !=0){
            // data["error"]=0;
            // data["user"]=rows;
            res.json(rows);
        }else{
            data["Data Siswa"]='Tidak ada data siswa...';
            res.json(data);
        }
    })
});

//  Mengambil Data detail siswa
router.get('/detailSiswa/(:id)',function (req,res, next) {
    var data = {
        "error": 1,
        "user": "Data tidak ada/kosong"
    };
        connection.query('SELECT tb_siswa.nis_lokal,tb_siswa.nisn,tb_siswa.nik,tb_siswa.nama,tb_siswa.ttl,tb_siswa.alamat,tb_siswa.foto,tb_kelas.kelas,tb_siswa.jenis_kelamin,tb_siswa.agama FROM tb_siswa JOIN tb_kelas ON tb_siswa.id_kelas = tb_kelas.id_kelas WHERE id_user = ' + req.params.id, function(err, rows, fields) {
            if(err) throw err

            // if user not found
            if (rows.length <= 0) {
                req.flash('error', 'User not found with id = ' + req.params.id)
                res.redirect('/users')
            }
            else { // if user found
                // render to views/user/edit.ejs template file
                res.json(rows);
            }
        })
})

module.exports = router;